# File `cas-particuliers-v4.json`

## Creation

```shell script
jq 'del(.topics[].criteria[].criterium.references, .topics[].criteria[].criterium.tests, .topics[].criteria[].criterium.technicalNote, .topics[].criteria[].criterium.title)' ../RGAAv4/criteres.json > cas-particuliers-v4.json
```

## Content

* Only *particular cases* from RGAAv4
