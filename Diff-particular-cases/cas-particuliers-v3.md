# File `cas-particuliers-v3.json`

## Creation

* Once `cas-particuliers-v4.json` is created: `cp cas-particuliers-v4.json cas-particuliers-v3.json`
* All particular cases are manually replaced by those from RGAAv3, and put in Markdown format (converted with https://convert-tool.com/conversion/html-to-markdown) 

## Content

* Only *particular cases* from RGAAv4



