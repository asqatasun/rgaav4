# Analyse differences cas particuliers

## Méthodologie

* la comparaison est effectuée **DEPUIS** le rgaav3 **VERS** le rgaav4

## Vocabulaire

* un cas particuliers *supprimé* existait en v3 mais n'existe plus en v4
* un cas particuliers *modifié* possède un intitulé différent en v3 et en v4

## Thématique 1 Images ========================================================

### Cas particuliers identiques

* 1.3

### Cas particuliers supprimés

* 1.9

### Cas particuliers modifiés

* 1.8

## Thématique 3 Couleurs ======================================================

### Cas particuliers supprimés

* 3.4

### Cas particuliers modifiés

* 3.3

### Cas particuliers ajoutés

* 3.2

## Thématique 4 Multimedia ====================================================

### Cas particuliers supprimés

* 4.9
* 4.11
* 4.15
* 4.16
* 4.19
* 4.22

### Cas particuliers modifiés

* 4.1
* 4.2 (renvoie vers cas particulier 4.1)
* 4.3 (renvoie vers cas particulier 4.1)
* 4.5 (renvoie vers cas particulier 4.1)
* 4.7 (renvoie vers cas particulier 4.1)
* 4.13

### Cas particuliers ajoutés

* 4.8

## Thématique 5 Tableaux ======================================================

### Cas particuliers ajoutés

* 5.7

## Thématique 6 Liens =========================================================

### Cas particuliers modifiés

* 6.1

### Cas particuliers supprimés

* 6.3

## Thématique 7 Scripts =======================================================

### Cas particuliers identiques

* 7.3 (différences uniquement syntaxiques)

### Cas particuliers supprimés

* 7.6

### Cas particuliers ajoutés

* 7.1

## Thématique 8 Éléments obligatoires =========================================

### Cas particuliers modifiés

* 8.2
* 8.7

### Cas particuliers ajoutés

* 8.3

## Thématique 9 Structuration =================================================

### Cas particuliers ajoutés 

* 9.2

## Thématique 10 Présentation de l'information ================================

### Cas particuliers identiques

* 10.12

### Cas particuliers modifiés

* 10.11

### Cas particuliers ajoutés

* 10.4
* 10.13

## Thématique 11 Formulaires ==================================================

### Cas particuliers ajoutés

* 11.2
* 11.4
* 11.9
* 11.10

## Thématique 12 Navigation ===================================================

### Cas particuliers identiques

* 12.1 (différences uniquement syntaxiques)

### Cas particuliers modifiés

* 12.2

### Cas particuliers supprimés

* 12.3
* 12.8
* 12.10
* 12.11

### Cas particuliers ajoutés

* 12.7

## Thématique 13 Consultation =================================================

### Cas particuliers identiques

* 13.1 (différences uniquement syntaxiques)

### Cas particuliers ajoutés

* 13.3
* 13.4
* 13.9
* 13.10
* 13.11
* 13.12

### Cas particuliers supprimés

* 13.6
