# File `glossaire-v3.json`

## Creation

* Once `glossaire-v4-without-link.json` is created: `cp glossaire-v4-without-link.json glossaire-v3.json`
* All glossary entries are manually replaced by those from RGAAv3, and put in Markdown format (converted with https://convert-tool.com/conversion/html-to-markdown) 


## Content

* Only *glossary* from RGAAv3
