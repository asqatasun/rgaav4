# Analyse differences glossaire

## Méthodologie

* la comparaison est effectuée **DEPUIS** le rgaav3 **VERS** le rgaav4

## Vocabulaire

* une entrée de glossaire *supprimée* existait en v3 mais n'existe plus en v4
* une entrée de glossaire *modifiée* possède un intitulé différent en v3 et en v4

## Lettre A ===================================================================

### Entrées de glossaire identiques

* `Accéder à chaque page de la collection de pages`
* `Alerte` (différences syntaxiques uniquement)
* `Alternative (à script)` (différences syntaxiques uniquement)
* `Alternative courte et concise` (différences syntaxiques uniquement)
* `Ambigu pour tout le monde` (différences syntaxiques uniquement)
* `Ancre` (différences syntaxiques uniquement)

### Entrées de glossaire supprimées

* `Adapter un motif de conception ARIA`
* `Arborescence du document`
* `Audio-description étendue`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Accessible et activable par le clavier et la souris` --> `Accessible et activable par le clavier et tout dispositif de pointage`
* `Alternative textuelle (image)` Modification majeure (nombreuses et en profondeur)
* `Audiodescription synchronisée (média temporel)` Modification mineure

### Entrées de glossaire ajoutées

* `Accolés (étiquette et champ accolés)`
* `Alternative à une image SVG`

## Lettre B ===================================================================

### Entrées de glossaire supprimées

* `Bloc d'informations de même nature`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Barre de navigation` --> `Menu et barre de navigation` Modifications mineures
* `Bouton (formulaire)` Modification majeure

## Lettre C ===================================================================
 
### Entrées de glossaire identiques

* `CAPTCHA`
* `Changement brusque de luminosité ou effet de flash`
* `Changement de contexte`
* `Changement de langue`
* `Code de langue`
* `Compatible avec les technologies d'assistance`
* `Compréhensible (ordre de lecture)`
* `Contenu visible`
* `Contraste`
* `Contrôle (contenu en mouvement ou clignotant)`
* `Contrôle (son déclenché automatiquement)`

### Entrées de glossaire supprimées

* `Code valide`
* `Collection de pages`
* `Contrôlable par le clavier et la souris`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Cadre en ligne` --> `Cadre`
* `Champ de saisie de formulaire`
* `Contexte du lien` 
* `Contrôle de la consultation (d'un média temporel)`
* `Contrôle de saisie (formulaire)`

### Entrées de glossaire ajoutées

* `Champs de même nature`
* `Composant d'interface`
* `Contenu alternatif`
* `Contenu caché`
* `Couleur d'arrière-plan contiguë et couleur contiguë`

## Lettre D ===================================================================
 
### Entrées de glossaire modifiées (titre et/ou contenu)

* `Description détaillée (image)`

## Lettre E ===================================================================
 
### Entrées de glossaire identiques

* `Environnement maîtrisé`

### Entrées de glossaire supprimées
### Entrées de glossaire modifiées (titre et/ou contenu)

* `Ensemble de pages` Une modification mineure (a priori sans impact sur les règles)
* `En-tête de colonne ou de ligne` Modification mineure
* `Étiquette de champ de formulaire` Modification majeure
 
### Entrées de glossaire ajoutées

* `Élément graphique`
* `Étiquettes cohérentes`

## Lettre F ===================================================================
 
### Entrées de glossaire identiques

* `Feuille de style`

### Entrées de glossaire supprimées

* `Fonctionnalités de contrôle (media temporel)`

### Entrées de glossaire ajoutées

* `Formulaire`

## Lettre G =================================================================== 

### Entrées de glossaire ajoutées

* `Gestes complexes et gestes simples`

## Lettre I ===================================================================
 
### Entrées de glossaire identiques

* `Image objet`
* `Image réactive`
* `Image-test`
* `Image texte`
* `Image texte objet`
* `Image véhiculant une information (donnée par la couleur)`
* `Indication donnée par la forme, la taille ou la position`

### Entrées de glossaire supprimées
### Entrées de glossaire modifiées (titre et/ou contenu)

* `Image de décoration`
* `Image porteuse d'information`
* `Information (donnée par la couleur)`
* `Intitulé de lien` --> `Intitulé (ou nom accessible) de lien"` Modification majeure

### Entrées de glossaire ajoutées

* `Indication de format obligatoire`
* `Indication du type de données et/ou de format`
* `Intitulé visible`
* `Items de même nature des listes de choix`

## Lettre J ===================================================================  

### Entrées de glossaire supprimées

* `Jusqu'à ou à partir de 150% (1.5em) de la taille de police par défaut sans effet de graisse. Jusqu'à ou à partir de 120% (1.2em) de la taille de police par défaut en gras`

## Lettre L ===================================================================

### Entrées de glossaire identiques

* `Langue par défaut`
* `Lien dont la nature n'est pas évidente`
* `Liens identiques`

### Entrées de glossaire supprimées

* `Lien explicite hors contexte`
* `Liste de choix`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Le nom, le rôle, la valeur, le paramétrage et les changements d'états`
* `Légende d'image` modification mineure (a priori sans impact pour nous)
* `Lien` Modification majeure
* `Lien adjacent` --> `Lien ou bouton adjacent`
* `Lien composite`
* `Lien image` Modification majeure
* `Lien texte` Modification majeure
* `Lien vectoriel` --> `Lien SVG` 
* `Liens d'évitement ou d'accès rapide`
* `Listes` Modification majeure

### Entrées de glossaire ajoutées

* `Landmarks`
* `Légende`
* `Liste des valeurs possibles pour l'attribut autocomplete`

## Lettre M ===================================================================

### Entrées de glossaire identiques

* `Mécanisme de remplacement`
* `Média non temporel` Modification syntaxique et de cohérence uniquement
* `Moteur de recherche (interne à un site web)`

### Entrées de glossaire supprimées

* `Menu de navigation`
* `Modification du rôle natif d'un élément HTML`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Média temporel (type son, vidéo et synchronisé)` Modification mineure
* `Motif de conception`

### Entrées de glossaire ajoutées

* `Mécanisme qui permet d'afficher un rapport de contraste conforme`
* `Message de statut`
* `Modifier ou annuler les données et les actions effectués`

## Lettre O ===================================================================

### Entrées de glossaire identiques

* `Ordre de tabulation`


## Lettre P ===================================================================

### Entrées de glossaire identiques

* `Page « plan du site »`
* `Pertinence (information autrement que par la couleur)`
* `Procédé de rafraîchissement`
* `Propriété CSS déterminant une couleur`

### Entrées de glossaire supprimées

* `Propriétés et méthodes conformes à la spécification DOM`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Présentation de l'information`
* `Prise de focus`

### Entrées de glossaire ajoutées

* `Passage de texte associé au tableau de données`
* `Passage de texte lié par aria-labelledby ou aria-describedby`
* `Pressé ou posé`

## Lettre R ===================================================================

### Entrées de glossaire identiques

* `Correctement restitué (par les technologies d'assistance)`

### Entrées de glossaire supprimées

* `Rôle WAI-ARIA équivalent (surcharge de rôle)`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Redirection automatique` --> `Redirection` Modification mineure
* `Résumé (de tableau)`
    
### Entrées de glossaire ajoutées

* `Raccourci clavier`
* `Règles d'écriture`
* `Relâché ou relevé`

## Lettre S ===================================================================

### Entrées de glossaire identiques

* `Script`
* `Site web : ensemble de toutes les pages web`
* `Système de navigation`

### Entrées de glossaire supprimées
### Entrées de glossaire modifiées (titre et/ou contenu)

* `Sens de lecture`
* `Sous-titres synchronisés (objet multimédia)` Modification mineure (a priori sans impact pour nous)
 
### Entrées de glossaire ajoutées

* `Si nécessaire (texte visible en complément de l'attribut aria-label ou aria-labelledby)`
* `Structure du document`

## Lettre T ===================================================================

### Entrées de glossaire identiques

* `Tableau de mise en forme`
* `Texte stylé`
* `Titre de page`
* `Transcription textuelle (média temporel)`
* `Type de document`
* `Type et format de données`

### Entrées de glossaire supprimées

* `Texte caché`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Tableau de données`
* `Tableau de données complexe`
* `Taille des caractères` 
* `Titre`
* `Titre d'un tableau (de données)` --> `Tableau de données ayant un titre` 
* `Titre de cadre` modification mineure

### Entrées de glossaire ajoutées

* `Titre de lien`

## Lettre U ===================================================================

### Entrées de glossaire identiques

* `URL`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Uniquement à des fins de présentation`

## Lettre V ===================================================================

### Entrées de glossaire identiques

* `Version accessible (pour un document en téléchargement)`
* `Version alternative « audio seulement »`

## Lettre Z ===================================================================

### Entrées de glossaire identiques

* `Zone cliquable`
* `Zone d'en-tête`
* `Zone de contenu principal`
* `Zone de pied de page`

### Entrées de glossaire modifiées (titre et/ou contenu)

* `Zone (d'une image réactive)` modification mineure
* `Zone non cliquable`

### Entrées de glossaire ajoutées

* `Zone texte`
