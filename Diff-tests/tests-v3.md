# File `tests-v3.json`

## Creation

* Once `tests-v4.json` is created: `cp tests-v4.json tests-v3.json`
* All tests are manually replaced by those from RGAAv3, and put in Markdown format (converted with https://convert-tool.com/conversion/html-to-markdown) 

## Content

* Only *tests* from RGAAv4
