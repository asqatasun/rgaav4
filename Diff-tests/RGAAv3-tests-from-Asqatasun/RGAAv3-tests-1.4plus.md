
"1": [
"Chaque image (balise `img`) utilisée comme CAPTCHA ou comme image-test, ayant un attribut `alt`, vérifie-t-elle ces conditions ?",
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction de l'image",
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
]
"2": [
"Chaque zone (balise `area`) d'une image réactive, utilisée comme CAPTCHA ou comme image-test, et ayant un attribut `alt` vérifie-t-elle ces conditions ?",
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction de la zone",
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
]
"3": [
"Chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) utilisée comme CAPTCHA ou comme image-test, ayant un attribut `alt` vérifie-t-il ces conditions ?",
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction du bouton",
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
]
"4": [
"Pour chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) utilisée comme CAPTCHA ou comme image-test, et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
]
"5": [
"Pour chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) utilisée comme CAPTCHA ou comme image-test, et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
]
"6": [
"Chaque image vectorielle (balise `svg`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle via la propriété `aria-label` ou la balise `desc` vérifie-t-elle ces conditions ?",
"l'alternative textuelle implémentée via la propriété `aria-label` permet d'identifier la nature et la fonction de l'image et est identique à l'attribut `title` s'il est présent",
"l'alternative textuelle implémentée via la balise `desc` permet d'identifier la nature et la fonction de l'image et est identique à l'attribut `title` de la balise `svg` s'il est présent"
]
"7": [
"Pour chaque image vectorielle (balise `svg`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle , l'alternative textuelle est-elle correctement restituée par les technologies d'assistance ?"
]
"8": [
"Pour chaque image bitmap (balise `canvas`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
]
"9": [
"Pour chaque image bitmap (balise `canvas`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle , l'alternative textuelle est-elle correctement restituée par les technologies d'assistance ?"
]
"1": [
"Chaque image (balises `img`, `area`, `object`, `embed`, `svg`, `canvas`) utilisée comme CAPTCHA vérifie-t-elle une de ces conditions ?",
"Il existe une autre forme de CAPTCHA non graphique, au moins",
"Il existe une autre solution d'accès à la fonctionnalité sécurisée par le CAPTCHA"
]
"2": [
"Chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) utilisée comme CAPTCHA vérifie-t-il une de ces conditions ?",
"Il existe une autre forme de CAPTCHA non graphique, au moins",
"Il existe une autre solution d'accès à la fonctionnalité sécurisée par le CAPTCHA"
]
"1": [
"Chaque image porteuse d'information (balise `img`) qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?",
"Il existe un attribut `longdesc` qui donne l'adresse (`url`) d'une page contenant la description détaillée",
"Il existe un attribut `alt` contenant la référence à une description détaillée adjacente à l'image",
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
]
"2": [
"Chaque image objet porteuse d'information (balise `object` avec l'attribut `type=\"image/...\"`), qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?",
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée",
"Il existe une description détaillée clairement identifiable adjacente à l'image objet"
]
"3": [
"Chaque image embarquée porteuse d'information (balise `embed`), qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?",
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée",
"Il existe une description détaillée clairement identifiable adjacente à l'image embarquée"
]
"4": [
"Chaque bouton de formulaire de type image (balise `input` avec l'attribut `type=\"image\"`), qui nécessite une description détaillée, vérifie-t-il une de ces conditions ?",
"Il existe un attribut `alt` contenant la référence à une description détaillée adjacente à l'image",
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
]
"5": [
"Chaque image vectorielle (balise `svg`) qui nécessite une description détaillée vérifie-t-elle une de ces conditions ?",
"Il existe une propriété `aria-label` contenant une référence à une description détaillée adjacente à l'image vectorielle",
"Il existe une balise `desc` contenant une référence à une description détaillée adjacente à l'image vectorielle",
"Il existe une balise `desc` contenant la description détaillée",
"Il existe un lien adjacent (via une balise `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
]
"6": [
"Pour chaque image vectorielle (balise `svg`) qui implémente une référence à une description détaillée adjacente via une propriété `aria-label` ou une balise `desc`, cette référence est-elle correctement restituée par les technologies d'assistance ?"
]
"7": [
"Chaque image bitmap (balise `canvas`) qui nécessite une description détaillée vérifie-t-elle une de ces conditions ?",
"Il existe un passage de texte entre `<canvas>` et `</canvas>` contenant une référence à une description détaillée adjacente à l'image bitmap",
"Il existe un contenu textuel entre `<canvas>` et `</canvas>` faisant office de description détaillée.",
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
]
"8": [
"Pour chaque image bitmap (balise `canvas`) qui implémente une référence à une description détaillée adjacente, cette référence est-elle correctement restituée par les technologies d'assistance ?"
]
"1": [
"Chaque image porteuse d'information (balise `img` ou `input` avec l'attribut `type=\"image\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?",
"La description détaillée via l'adresse référencée dans l'attribut `longdesc` est pertinente",
"La description détaillée dans la page et signalée dans l'attribut `alt` est pertinente",
"La description détaillée via un lien adjacent est pertinente"
]
"2": [
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?",
"La description détaillée adjacente à l'image objet est pertinente",
"La description détaillée via un lien adjacent est pertinente"
]
"3": [
"Chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?",
"La description détaillée adjacente à l'image embarquée est pertinente",
"La description détaillée via un lien adjacent est pertinente"
]
"4": [
"Chaque image vectorielle (balise `svg`) ayant une description détaillée vérifie-t-elle une de ces conditions ?",
"La description détaillée adjacente à l'image vectorielle est pertinente",
"La description détaillée contenue dans la balise `desc` est pertinente",
"La description détaillée via un lien adjacent est pertinente"
]
"5": [
"Pour chaque image vectorielle (balise `svg`) ayant une description détaillée implémentée via la balise `desc`, cette description détaillée est-elle correctement restituée par les technologies d'assistance ?"
]
"6": [
"Chaque image bitmap (balise `canvas`) ayant une description détaillée vérifie-t-elle une de ces conditions ?",
"La description détaillée adjacente à l'image bitmap est pertinente",
"La description détaillée contenue entre `<canvas>` et `</canvas>` est pertinente",
"La description détaillée via un lien adjacent est pertinente"
]
"7": [
"Pour chaque image bitmap (balise `canvas`) ayant une description détaillée entre `<canvas>` et `</canvas>`, cette description détaillée est-elle correctement restituée par les technologies d'assistance ?"
]
"1": [
"Chaque image texte (balise `img`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"2": [
"Pour chaque image réactive (balise `img` ou `object` avec l'attribut `usemap` ou l'attribut `ismap`), chaque zone texte (balise `area` ou zone cliquable) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"3": [
"Pour chaque balise `form`, chacun de ses boutons \"image texte\" (balise `input` avec l'attribut `type=\"image\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacé par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"4": [
"Chaque image texte objet (balise `object` avec l'attribut `type=\"image/...\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"5": [
"Chaque image texte embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"6": [
"Chaque image texte bitmap (balise `canvas`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"1": [
"Chaque image texte (balise `img`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"2": [
"Pour chaque image réactive (balise `img` ou `object` avec l'attribut `usemap`), chaque zone texte (balise `area`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"3": [
"Pour chaque balise `form`, chacun de ses boutons \"image texte\" (balise `input` avec l'attribut `type=\"image\"`) doit si possible être remplacé par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"4": [
"Chaque image texte objet (balise `object` avec l'attribut `type=\"image/...\"`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"5": [
"Chaque image texte embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"6": [
"Chaque image texte bitmap (balise `canvas`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
]
"1": [
"Chaque image légendée (balise `img` ou `input` avec l'attribut `type=\"image\"` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?",
"L'image (balise `img`) et sa légende sont contenues dans une balise `figure`",
"la balise `figure` possède un attribut `role=\"group\"`",
"Le contenu de l'attribut `alt` de l'image contient une référence à la légende adjacente",
"L'attribut `title` de l'image s'il est présent, est strictement identique au contenu de l'attribut `alt`"
],
"2": [
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?",
"L'image (balise `object`) et sa légende sont contenues dans une balise `figure`",
"la balise `figure` possède un attribut `role=\"group\"`"
],
"3": [
"Chaque image embarquée légendée (balise `embed` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?",
"L'image embarquée (balise `embed`) et sa légende sont contenues dans une balise `figure`",
"la balise `figure` possède un attribut `role=\"group\"`",
"Le contenu de l'attribut `alt` de l'image contient une référence à la légende adjacente",
"L'attribut `title` de l'image s'il est présent, est strictement identique au contenu de l'attribut `alt`"
],
"4": [
"Chaque image vectorielle légendée (balise `svg` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?",
"L'image (balise `svg`) et sa légende sont contenues dans une balise `figure`",
"la balise `figure` possède un `role=\"group\"`",
"Le contenu de la propriété `aria-label` ou de la balise `desc` de l'image vectorielle contient une référence à la légende adjacente",
"L'attribut `title` de l'image vectorielle (balise `svg`) s'il est présent, est strictement identique au contenu de la propriété `aria-label` ou de la balise `desc` utilisé comme alternative."
],
"5": [
"Chaque image bitmap légendée (balise `canvas` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?",
"L'image (balise `canvas`) et sa légende sont contenues dans une balise `figure`",
"la balise `figure` possède un attribut `role=\"group\"`",
"Le contenu entre `<canvas>` et `</canvas>` de l'image bitmap contient une référence à la légende adjacente",
]
