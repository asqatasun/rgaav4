"1": [
"Chaque image de décoration sans légende (balise `img`) et ayant un attribut `alt` vérifie-t-elle ces conditions : ",
"le contenu de l'attribut alt est vide (`alt=\"\"`)",
"l'image de décoration ne possède pas d'attribut `title`"
],
"2": [
"Chaque zone non cliquable (balise `area` sans attribut `href`), non porteuse d'information et ayant un attribut `alt` vérifie-t-elle ces conditions ?",
"le contenu de l'attribut `alt` est vide (`alt=\"\"`)",
"la zone cliquable ne possède pas d'attribut `title`"
],
"3": "Pour chaque image objet sans légende (balise `object` avec l'attribut `type=\"image/...\"`) non porteuse d'information, l'alternative textuelle entre `<object>` et `</object>` est-elle vide ?",
"4": [
"Chaque image vectorielle de décoration (balise `svg`) non porteuse d'information et possédant une alternative vérifie-t-elle ces conditions ?",
"La balise `svg` possède un `role=\"img\"`",
"La balise `svg` ou l'un de ses enfants est dépourvue de role, propriété ou état ARIA visant à labelliser l'image vectorielle (`aria-label`, `aria-describedby`, `aria-labelledby` par exemple).",
"Les balises `title` et `desc` sont absentes ou vides",
"La balise `svg` ou l'un de ses enfants est dépourvue d'attribut `title`"
],
"5": "Pour chaque image bitmap de décoration (balise `canvas`), le contenu entre `<canvas>` et `</canvas>` doit être dépourvu de contenus textuels, cette règle est-elle respectée ?",
