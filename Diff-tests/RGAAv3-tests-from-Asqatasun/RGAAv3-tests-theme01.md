Rgaa30-1-1-1=
"Chaque image (balise `img`) a-t-elle un attribut `alt` ?"
Rgaa30-1-1-2=
"Chaque zone (balise `area`) d'une image réactive a-t-elle un attribut `alt` ?"
Rgaa30-1-1-3=
"Chaque bouton de formulaire (balise `input` avec l'attribut `type=\"image\"`) a-t-il un attribut `alt` ?"
Rgaa30-1-1-4=
"Chaque zone (balise `area`) d'une image réactive coté serveur est-t-elle doublée d'un lien dans la page ?"
Rgaa30-1-2-1=
"Chaque image de décoration sans légende (balise `img`) et ayant un attribut `alt` vérifie-t-elle ces conditions : "
"le contenu de l'attribut alt est vide (`alt=\"\"`)"
"l'image de décoration ne possède pas d'attribut `title`"
Rgaa30-1-2-2=
"Chaque zone non cliquable (balise `area` sans attribut `href`), non porteuse d'information et ayant un attribut `alt` vérifie-t-elle ces conditions ?"
"le contenu de l'attribut `alt` est vide (`alt=\"\"`)"
"la zone cliquable ne possède pas d'attribut `title`"
Rgaa30-1-2-3=
"Pour chaque image objet sans légende (balise `object` avec l'attribut `type=\"image/...\"`) non porteuse d'information, l'alternative textuelle entre `<object>` et `</object>` est-elle vide ?"
Rgaa30-1-2-4=
"Chaque image vectorielle de décoration (balise `svg`) non porteuse d'information et possédant une alternative vérifie-t-elle ces conditions ?"
"La balise `svg` possède un `role=\"img\"`"
"La balise `svg` ou l'un de ses enfants est dépourvue de role, propriété ou état ARIA visant à labelliser l'image vectorielle (`aria-label`, `aria-describedby`, `aria-labelledby` par exemple)."
"Les balises `title` et `desc` sont absentes ou vides"
"La balise `svg` ou l'un de ses enfants est dépourvue d'attribut `title`"
Rgaa30-1-2-5=
"Pour chaque image bitmap de décoration (balise `canvas`), le contenu entre `<canvas>` et `</canvas>` doit être dépourvu de contenus textuels, cette règle est-elle respectée ?"
Rgaa30-1-3-1=
"Chaque image porteuse d'information (balise `img`) ayant un attribut `alt` vérifie-t-elle ces conditions (hors cas particuliers) ?"
"Le contenu de l'attribut `alt` est pertinent"
"S'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
Rgaa30-1-3-2=
"Chaque zone (balise area) d'une image réactive, porteuse d'information et ayant un attribut `alt`, vérifie-t-elle ces conditions (hors cas particuliers) ?"
"Le contenu de l'attribut `alt` est pertinent"
"S'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
Rgaa30-1-3-3=
"Pour chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) ayant un attribut `alt`, le contenu de cet attribut est-il pertinent (hors cas particuliers) ?"
Rgaa30-1-3-4=
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) porteuse d'information vérifie-t-elle une de ces conditions(hors cas particuliers) ?"
"L'image objet est immédiatement suivie d'un lien adjacent permettant d'afficher une page ou un passage de texte contenant une alternative pertinente."
"Un mécanisme permet à l'utilisateur de remplacer l'image objet par un texte alternatif pertinent"
"Un mécanisme permet à l'utilisateur de remplacer l'image objet par une image possédant une alternative pertinente."
Rgaa30-1-3-5=
"Chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) porteuse d'information vérifie-t-elle une de ces conditions (hors cas particuliers) ?"
"L'image embarquée est immédiatement suivie d'un lien adjacent permettant d'afficher une page ou un passage de texte contenant une alternative pertinente."
"Un mécanisme permet à l'utilisateur de remplacer l'image embarquée par un texte alternatif pertinent"
"Un mécanisme permet à l'utilisateur de remplacer l'image embarquée par une image possédant une alternative pertinente."
Rgaa30-1-3-6=
"Chaque image vectorielle porteuse d'information (balise `svg`) et possédant une alternative vérifie-t-elle une de ces conditions (hors cas particuliers) ?"
"La balise `svg` possède un `role=\"img\"`"
"La balise `svg` possède une propriété `aria-label` dont le contenu est pertinent et identique à l'attribut `title` s'il est présent"
"La balise `svg` possède une balise `desc` dont le contenu est pertinent et identique à l'attribut `title` de la balise `svg` s'il est présent"
"Un lien adjacent permet d'accéder à une alternative dont le contenu est pertinent et identique à l'attribut `title` de la balise `svg` s'il est présent"
Rgaa30-1-3-7=
"Pour chaque image vectorielle porteuse d'information (balise `svg`) et possédant une alternative, cette alternative est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-3-8=
"Pour chaque image bitmap porteuse d'information (balise `canvas`) et possédant une alternative (contenu entre `<canvas>` et `</canvas>`), cette alternative est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-3-9=
"Pour chaque image bitmap porteuse d'information (balise `canvas`) et possédant une alternative (contenu entre `<canvas>` et `</canvas>`), cette alternative est-elle pertinente ?"
Rgaa30-1-3-10=
"Pour chaque image porteuse d'information et ayant une alternative textuelle, l'alternative textuelle est-elle courte et concise (hors cas particuliers) ?"
Rgaa30-1-4-1=
"Chaque image (balise `img`) utilisée comme CAPTCHA ou comme image-test, ayant un attribut `alt`, vérifie-t-elle ces conditions ?"
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction de l'image"
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
Rgaa30-1-4-2=
"Chaque zone (balise `area`) d'une image réactive, utilisée comme CAPTCHA ou comme image-test, et ayant un attribut `alt` vérifie-t-elle ces conditions ?"
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction de la zone"
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
Rgaa30-1-4-3=
"Chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) utilisée comme CAPTCHA ou comme image-test, ayant un attribut `alt` vérifie-t-il ces conditions ?"
"le contenu de l'attribut `alt` permet d'identifier la nature et la fonction du bouton"
"s'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
Rgaa30-1-4-4=
"Pour chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) utilisée comme CAPTCHA ou comme image-test, et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
Rgaa30-1-4-5=
"Pour chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) utilisée comme CAPTCHA ou comme image-test, et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
Rgaa30-1-4-6=
"Chaque image vectorielle (balise `svg`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle via la propriété `aria-label` ou la balise `desc` vérifie-t-elle ces conditions ?"
"l'alternative textuelle implémentée via la propriété `aria-label` permet d'identifier la nature et la fonction de l'image et est identique à l'attribut `title` s'il est présent"
"l'alternative textuelle implémentée via la balise `desc` permet d'identifier la nature et la fonction de l'image et est identique à l'attribut `title` de la balise `svg` s'il est présent"
Rgaa30-1-4-7=
"Pour chaque image vectorielle (balise `svg`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle , l'alternative textuelle est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-4-8=
"Pour chaque image bitmap (balise `canvas`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle, l'alternative textuelle permet-elle d'identifier la nature et la fonction de l'image ?"
Rgaa30-1-4-9=
"Pour chaque image bitmap (balise `canvas`) utilisée comme CAPTCHA ou comme image-test et ayant une alternative textuelle , l'alternative textuelle est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-5-1=
"Chaque image (balises `img`, `area`, `object`, `embed`, `svg`, `canvas`) utilisée comme CAPTCHA vérifie-t-elle une de ces conditions ?"
"Il existe une autre forme de CAPTCHA non graphique, au moins"
"Il existe une autre solution d'accès à la fonctionnalité sécurisée par le CAPTCHA"
Rgaa30-1-5-2=
"Chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) utilisée comme CAPTCHA vérifie-t-il une de ces conditions ?"
"Il existe une autre forme de CAPTCHA non graphique, au moins"
"Il existe une autre solution d'accès à la fonctionnalité sécurisée par le CAPTCHA"
Rgaa30-1-6-1=
"Chaque image porteuse d'information (balise `img`) qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?"
"Il existe un attribut `longdesc` qui donne l'adresse (`url`) d'une page contenant la description détaillée"
"Il existe un attribut `alt` contenant la référence à une description détaillée adjacente à l'image"
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
Rgaa30-1-6-2=
"Chaque image objet porteuse d'information (balise `object` avec l'attribut `type=\"image/...\"`), qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?"
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
"Il existe une description détaillée clairement identifiable adjacente à l'image objet"
Rgaa30-1-6-3=
"Chaque image embarquée porteuse d'information (balise `embed`), qui nécessite une description détaillée, vérifie-t-elle une de ces conditions ?"
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
"Il existe une description détaillée clairement identifiable adjacente à l'image embarquée"
Rgaa30-1-6-4=
"Chaque bouton de formulaire de type image (balise `input` avec l'attribut `type=\"image\"`), qui nécessite une description détaillée, vérifie-t-il une de ces conditions ?"
"Il existe un attribut `alt` contenant la référence à une description détaillée adjacente à l'image"
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
Rgaa30-1-6-5=
"Chaque image vectorielle (balise `svg`) qui nécessite une description détaillée vérifie-t-elle une de ces conditions ?"
"Il existe une propriété `aria-label` contenant une référence à une description détaillée adjacente à l'image vectorielle"
"Il existe une balise `desc` contenant une référence à une description détaillée adjacente à l'image vectorielle"
"Il existe une balise `desc` contenant la description détaillée"
"Il existe un lien adjacent (via une balise `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
Rgaa30-1-6-6=
"Pour chaque image vectorielle (balise `svg`) qui implémente une référence à une description détaillée adjacente via une propriété `aria-label` ou une balise `desc`, cette référence est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-6-7=
"Chaque image bitmap (balise `canvas`) qui nécessite une description détaillée vérifie-t-elle une de ces conditions ?"
"Il existe un passage de texte entre `<canvas>` et `</canvas>` contenant une référence à une description détaillée adjacente à l'image bitmap"
"Il existe un contenu textuel entre `<canvas>` et `</canvas>` faisant office de description détaillée."
"Il existe un lien adjacent (via une `url` ou une `ancre`) permettant d'accéder au contenu de la description détaillée"
Rgaa30-1-6-8=
"Pour chaque image bitmap (balise `canvas`) qui implémente une référence à une description détaillée adjacente, cette référence est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-7-1=
"Chaque image porteuse d'information (balise `img` ou `input` avec l'attribut `type=\"image\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?"
"La description détaillée via l'adresse référencée dans l'attribut `longdesc` est pertinente"
"La description détaillée dans la page et signalée dans l'attribut `alt` est pertinente"
"La description détaillée via un lien adjacent est pertinente"
Rgaa30-1-7-2=
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?"
"La description détaillée adjacente à l'image objet est pertinente"
"La description détaillée via un lien adjacent est pertinente"
Rgaa30-1-7-3=
"Chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) ayant une description détaillée vérifie-t-elle une de ces conditions ?"
"La description détaillée adjacente à l'image embarquée est pertinente"
"La description détaillée via un lien adjacent est pertinente"
Rgaa30-1-7-4=
"Chaque image vectorielle (balise `svg`) ayant une description détaillée vérifie-t-elle une de ces conditions ?"
"La description détaillée adjacente à l'image vectorielle est pertinente"
"La description détaillée contenue dans la balise `desc` est pertinente"
"La description détaillée via un lien adjacent est pertinente"
Rgaa30-1-7-5=
"Pour chaque image vectorielle (balise `svg`) ayant une description détaillée implémentée via la balise `desc`, cette description détaillée est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-7-6=
"Chaque image bitmap (balise `canvas`) ayant une description détaillée vérifie-t-elle une de ces conditions ?"
"La description détaillée adjacente à l'image bitmap est pertinente"
"La description détaillée contenue entre `<canvas>` et `</canvas>` est pertinente"
"La description détaillée via un lien adjacent est pertinente"
Rgaa30-1-7-7=
"Pour chaque image bitmap (balise `canvas`) ayant une description détaillée entre `<canvas>` et `</canvas>`, cette description détaillée est-elle correctement restituée par les technologies d'assistance ?"
Rgaa30-1-8-1=
"Chaque image texte (balise `img`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-8-2=
"Pour chaque image réactive (balise `img` ou `object` avec l'attribut `usemap` ou l'attribut `ismap`), chaque zone texte (balise `area` ou zone cliquable) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-8-3=
"Pour chaque balise `form`, chacun de ses boutons \"image texte\" (balise `input` avec l'attribut `type=\"image\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacé par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-8-4=
"Chaque image texte objet (balise `object` avec l'attribut `type=\"image/...\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-8-5=
"Chaque image texte embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-8-6=
"Chaque image texte bitmap (balise `canvas`) porteuse d'information, en l'absence d'un mécanisme de remplacement, doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-1=
"Chaque image texte (balise `img`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-2=
"Pour chaque image réactive (balise `img` ou `object` avec l'attribut `usemap`), chaque zone texte (balise `area`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-3=
"Pour chaque balise `form`, chacun de ses boutons \"image texte\" (balise `input` avec l'attribut `type=\"image\"`) doit si possible être remplacé par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-4=
"Chaque image texte objet (balise `object` avec l'attribut `type=\"image/...\"`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-5=
"Chaque image texte embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-9-6=
"Chaque image texte bitmap (balise `canvas`) doit si possible être remplacée par du texte stylé. Cette règle est-elle respectée (hors cas particuliers) ?"
Rgaa30-1-10-1=
"Chaque image légendée (balise `img` ou `input` avec l'attribut `type=\"image\"` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?"
"L'image (balise `img`) et sa légende sont contenues dans une balise `figure`"
"la balise `figure` possède un attribut `role=\"group\"`"
"Le contenu de l'attribut `alt` de l'image contient une référence à la légende adjacente"
"L'attribut `title` de l'image s'il est présent, est strictement identique au contenu de l'attribut `alt`"
Rgaa30-1-10-2=
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?"
"L'image (balise `object`) et sa légende sont contenues dans une balise `figure`"
"la balise `figure` possède un attribut `role=\"group\"`"
Rgaa30-1-10-3=
"Chaque image embarquée légendée (balise `embed` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?"
"L'image embarquée (balise `embed`) et sa légende sont contenues dans une balise `figure`"
"la balise `figure` possède un attribut `role=\"group\"`"
"Le contenu de l'attribut `alt` de l'image contient une référence à la légende adjacente"
"L'attribut `title` de l'image s'il est présent, est strictement identique au contenu de l'attribut `alt`"
Rgaa30-1-10-4=
"Chaque image vectorielle légendée (balise `svg` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?"
"L'image (balise `svg`) et sa légende sont contenues dans une balise `figure`"
"la balise `figure` possède un `role=\"group\"`"
"Le contenu de la propriété `aria-label` ou de la balise `desc` de l'image vectorielle contient une référence à la légende adjacente"
"L'attribut `title` de l'image vectorielle (balise `svg`) s'il est présent, est strictement identique au contenu de la propriété `aria-label` ou de la balise `desc` utilisé comme alternative."
Rgaa30-1-10-5=
"Chaque image bitmap légendée (balise `canvas` associée à une légende adjacente) vérifie-t-elle, si nécessaire, ces conditions ?"
"L'image (balise `canvas`) et sa légende sont contenues dans une balise `figure`"
"la balise `figure` possède un attribut `role=\"group\"`"
"Le contenu entre `<canvas>` et `</canvas>` de l'image bitmap contient une référence à la légende adjacente"
