"1": "Chaque image (balise `img`) a-t-elle un attribut `alt` ?"
"2": "Chaque zone (balise `area`) d'une image réactive a-t-elle un attribut `alt` ?"
"3": "Chaque bouton de formulaire (balise `input` avec l'attribut `type=\"image\"`) a-t-il un attribut `alt` ?"
"4": "Chaque zone (balise `area`) d'une image réactive coté serveur est-t-elle doublée d'un lien dans la page ?"
