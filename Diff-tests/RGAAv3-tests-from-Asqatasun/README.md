# README.md

## Objet

Avoir les tests RGAAv3 dans un format tel qu'on puisse les insérer dans une structure JSON et faire la comparaison avec le RGAAv4

## Construction

* Le fichier de départ est pris d'Asqatasun v4.1 : [rule-rgaa30-I18N_fr.properties](https://github.com/Asqatasun/Asqatasun/blob/v4.1.0/rules/rules-rgaa3.0/src/main/resources/i18n/rule-rgaa30-I18N_fr.properties)
* Ce fichier est transformé (par recherché/remplacé) pour avoir un format s'approchant du JSON

## Commandes utilisés (en vrac)

```shell script
perl -pi -e 's/Rgaa30-1-3-(\d+)\=\n/"$1": /' RGAAv3-tests-1.3.md
perl -pi -e 's/$/,/' RGAAv3-tests-1.3.md

perl -pi -e 's/$/,/'  RGAAv3-tests-1.4plus.md
perl -pi -e 's/Rgaa30-1-(\d+)-(\d+)=,/\n"$2": \[/' RGAAv3-tests-1.4plus.md
```
