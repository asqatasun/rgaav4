"1": [
"Chaque image porteuse d'information (balise `img`) ayant un attribut `alt` vérifie-t-elle ces conditions (hors cas particuliers) ?",
"Le contenu de l'attribut `alt` est pertinent",
"S'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
],
"2": [
"Chaque zone (balise area) d'une image réactive, porteuse d'information et ayant un attribut `alt`, vérifie-t-elle ces conditions (hors cas particuliers) ?",
"Le contenu de l'attribut `alt` est pertinent",
"S'il est présent, le contenu de l'attribut `title` est identique au contenu de l'attribut `alt`"
],
"3": "Pour chaque bouton associé à une image (balise `input` avec l'attribut `type=\"image\"`) ayant un attribut `alt`, le contenu de cet attribut est-il pertinent (hors cas particuliers) ?",
"4": [
"Chaque image objet (balise `object` avec l'attribut `type=\"image/...\"`) porteuse d'information vérifie-t-elle une de ces conditions(hors cas particuliers) ?",
"L'image objet est immédiatement suivie d'un lien adjacent permettant d'afficher une page ou un passage de texte contenant une alternative pertinente.",
"Un mécanisme permet à l'utilisateur de remplacer l'image objet par un texte alternatif pertinent",
"Un mécanisme permet à l'utilisateur de remplacer l'image objet par une image possédant une alternative pertinente."
],
"5": [
"Chaque image embarquée (balise `embed` avec l'attribut `type=\"image/...\"`) porteuse d'information vérifie-t-elle une de ces conditions (hors cas particuliers) ?",
"L'image embarquée est immédiatement suivie d'un lien adjacent permettant d'afficher une page ou un passage de texte contenant une alternative pertinente.",
"Un mécanisme permet à l'utilisateur de remplacer l'image embarquée par un texte alternatif pertinent",
"Un mécanisme permet à l'utilisateur de remplacer l'image embarquée par une image possédant une alternative pertinente."
],
"6": [
"Chaque image vectorielle porteuse d'information (balise `svg`) et possédant une alternative vérifie-t-elle une de ces conditions (hors cas particuliers) ?",
"La balise `svg` possède un `role=\"img\"`",
"La balise `svg` possède une propriété `aria-label` dont le contenu est pertinent et identique à l'attribut `title` s'il est présent",
"La balise `svg` possède une balise `desc` dont le contenu est pertinent et identique à l'attribut `title` de la balise `svg` s'il est présent",
"Un lien adjacent permet d'accéder à une alternative dont le contenu est pertinent et identique à l'attribut `title` de la balise `svg` s'il est présent"
],
"7": "Pour chaque image vectorielle porteuse d'information (balise `svg`) et possédant une alternative, cette alternative est-elle correctement restituée par les technologies d'assistance ?",
"8": "Pour chaque image bitmap porteuse d'information (balise `canvas`) et possédant une alternative (contenu entre `<canvas>` et `</canvas>`), cette alternative est-elle correctement restituée par les technologies d'assistance ?",
"9": "Pour chaque image bitmap porteuse d'information (balise `canvas`) et possédant une alternative (contenu entre `<canvas>` et `</canvas>`), cette alternative est-elle pertinente ?",
"10": "Pour chaque image porteuse d'information et ayant une alternative textuelle, l'alternative textuelle est-elle courte et concise (hors cas particuliers) ?",
