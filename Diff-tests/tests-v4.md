# File `tests-v4.json`

## Creation

```shell script
jq 'del(.topics[].criteria[].criterium.references, .topics[].criteria[].criterium.title, .topics[].criteria[].criterium.particularCases, .topics[].criteria[].criterium.technicalNote)' \
 ../RGAAv4/criteres.json > tests-v4.json
```

## Content

* Only *tests* from RGAAv4
