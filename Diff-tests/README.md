# Différence sur les tests RGAA v3 / v4

## RGAAv4 delta de présentation entre JSON et site web

Les listes à puce ne sont pas transcrites dans le JSON en tant que structure explicite.

Par contre pour un test donné en JSON, il semble que :

* le premier paragraphe est restitué en tant que paragraphe HTML (préfixé par le numéro du test)
* les paragraphes suivants sont restitués chacun comme élément d'une liste à puce.

