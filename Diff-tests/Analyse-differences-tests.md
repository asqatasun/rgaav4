# Analyse-differences-tests.md

## Méthodologie

* la comparaison est effectuée **DEPUIS** le rgaav3 **VERS** le rgaav4
* Dans un premier temps, l'ajout de lien vers le glossaire (au sein d'un intitulé de test) n'est **PAS** considéré comme une différence.

## Vocabulaire

* un test *supprimé* existait en v3 mais n'existe plus en v4
* un test *nouveau* n'existait pas en v3 et est apparu en v4
* un test *renuméroté* possède un même intitulé en v3 et en v4 (rappel : l'ajout de lien vers le glossaire n'est pas considéré comme une différence), mais son numéro a changé.
* un test *identique* possède un même numéro et un même intitulé (les différences relatives aux cas particuliers, notes techniques et glossaire seront traitées dans un deuxième temps)
* un test *modifié* possède un intitulé différent en v3 et en v4

##  Thématique 1 Images =======================================================

 tests | RGAAv3 | RGAAv4 | 
----------|-------:|-------:|
Nombre    |     x |      y |

### Tests supprimés

* 1.3
    * 1.3.7
* 1.4
    * 1.4.7
    * 1.4.9
* 1.7
    * 1.7.5
    * 1.7.7
* 1.8
    * 1.8.2
* 1.9
    * 1.9.1
    * 1.9.2
    * 1.9.3
    * 1.9.4
    * 1.9.5
    * 1.9.6
* 1.10
    * 1.10.1
    * 1.10.2
    * 1.10.3
    * 1.10.4
    * 1.10.5


### Tests renumérotés

* 1.3.10 devient 1.3.9
* 1.8.4 devient 1.8.3
* 1.8.5 devient 1.8.4
* 1.8.6 devient 1.8.5

### Tests renumérotés ET modifiés

* 1.3.9 devient 1.3.7 et est modifié
* 1.4.8 devient 1.4.7 et est modifié
* 1.7.2 devient 1.7.3 et est modifié
* 1.7.3 devient 1.7.4 et est modifié
* 1.7.4 devient 1.7.5 et est modifié
* 1.8.3 devient 1.8.2 et est modifié

### Tests nouveaux

* 1.1.5
* 1.1.6
* 1.1.7
* 1.1.8
* 1.2.6
* 1.6.9
* 1.6.10
* 1.9.1
* 1.9.2
* 1.9.3
* 1.9.4
* 1.9.5

### Tests identiques

* 1.1.4
* 1.5.2

### Tests modifiés

* 1.1
    * 1.1.1
    * 1.1.2
    * 1.1.3
* 1.2
    * 1.2.1
    * 1.2.2
    * 1.2.3
    * 1.2.4
    * 1.2.5
* 1.3
    * 1.3.1
    * 1.3.2
    * 1.3.3
    * 1.3.4
    * 1.3.5
    * 1.3.6
    * 1.3.8
* 1.4
    * 1.4.1
    * 1.4.2    
    * 1.4.3
    * 1.4.4
    * 1.4.5
    * 1.4.6
* 1.5
    * 1.5.1
* 1.6
    * 1.6.1
    * 1.6.2
    * 1.6.3
    * 1.6.4
    * 1.6.5
    * 1.6.6
    * 1.6.7
    * 1.6.8
* 1.7
    * 1.7.1
    * 1.7.6
* 1.8
    * 1.8.1

## Thématique 5 ===============================================================

## Thématique 6 ===============================================================

## Thématique 8 ===============================================================

* RGAAv3 8.8.1 + RGAAv3 8.8.2 sont fusionnés en RGAAv4 8.8.1 
* RGAAv3 8.10.1 est scindé en RGAAv4 8.10.1 + RGAAv4 8.10.2

## Thématique 9 ===============================================================

* RGAAv3 9.4.1 et RGAAv3 9.5.1 supprimé
* RGAAv3 9.6.1 renuméroté RGAAv4 9.4.1  
* RGAAv3 9.6.2 renuméroté RGAAv4 9.4.2  
