# File `notes-techniques-v4.json`

## Creation

```shell script
jq 'del(.topics[].criteria[].criterium.references, .topics[].criteria[].criterium.tests, .topics[].criteria[].criterium.particularCases, .topics[].criteria[].criterium.title)' ../RGAAv4/criteres.json > notes-techniques-v4.json
```

## Content

* Only *technical notes* from RGAAv4
