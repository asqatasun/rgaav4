# File `notes-techniques-v3.json`

## Creation

* Once `notes-techniques-v4.json` is created: `cp notes-techniques-v4.json notes-techniques-v3.json`
* All technical notes are manually replaced by those from RGAAv3, and put in Markdown format (converted with https://convert-tool.com/conversion/html-to-markdown) 

## Content

* Only *technical notes* from RGAAv4
