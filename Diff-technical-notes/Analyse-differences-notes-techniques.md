# Analyse differences notes techniques

## Méthodologie

* la comparaison est effectuée **DEPUIS** le rgaav3 **VERS** le rgaav4

## Vocabulaire

* un note technique *supprimé* existait en v3 mais n'existe plus en v4
* un note technique *modifié* possède un intitulé différent en v3 et en v4

## Thématique 1 Images =========================================================

### Notes techniques supprimés

* 1.1
* 1.3
* 1.10

### Notes techniques identiques

* 1.8

### Notes techniques modifiés

* 1.2
* 1.6

### Notes techniques ajoutées

* 1.9

## Thématique 5 tableaux ======================================================

### Notes techniques supprimés

* 5.1

### Notes techniques ajoutées

* 5.7

## Thématique 7 scripts =======================================================

### Notes techniques identiques

* 7.1 (quelques modifications mais syntaxiques uniquement)

### Notes techniques supprimés

* 7.3

### Notes techniques ajoutées

* 7.5

## Thématique 9 Structuration de l'information ================================

### Notes techniques identiques

* 9.1

### Notes techniques modifiés

* 9.2
* 9.3

### Notes techniques supprimés

* 9.4

## Thématique 10  =============================================================

### Notes techniques ajoutées

* 10.8

### Notes techniques supprimés

* 10.13

## Thématique 11 Formulaires ==================================================

### Notes techniques modifiés

* 11.11

### Notes techniques ajoutées

* 11.8
* 11.13

## Thématique 12 Navigation ===================================================

### Notes techniques supprimés

* 12.10

### Notes techniques ajoutées

* 12.11
