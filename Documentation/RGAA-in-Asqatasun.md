# RGAA in Asqatasun

## RGAA Structure

RGAA is structure in the following way:

* themes
    * criteria
        * test

## Vocabulary used in Asqatasun

*Test* is the object from the accessibility business.

Once a test is actually implemented in Asqatasun, we call it a *rule*.

Having two different words allows us to easily distinguish those two different objects.

## RGAA v3 figures 

* 13 themes
* 133 criterias
* 335 tests divided in:
    * 226 tests of level A
    * 48 tests of level AA
    * 61 tests of level AAA
* 173 rules divided in:
    * 118 rules of level A
    * 33 rules of level AA
    * 22 rules of level AAA

Among the 335 tests, 173 are implemented. So RGAA v3 in Asqatasun holds **173 rules**.

## RGAA v4 figures

* 13 themes
* 106 criterias
* 257 tests 
