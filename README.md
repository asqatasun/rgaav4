# RGAAv4

Planning for implementation of RGAAv4 in Asqatasun.

## Content organization

* Directory `RGAAv4` is a git submodule of https://github.com/DISIC/RGAA
* Directory `RGAAv3` is the RGAAv3 content recreated in JSON to ease diff between v3 and v4
* Directory `Documentation` gathers various notes and infos.
