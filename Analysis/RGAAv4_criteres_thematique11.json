{
  "topic": "Formulaires",
  "number": 11,
  "criteria": [
    {
      "criterium": {
        "number": 1,
        "title": "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) a-t-il une [étiquette](#etiquette-de-champ-de-formulaire) ?",
        "tests": {
          "1": [
            "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) vérifie-t-il une de ces conditions ?",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut WAI-ARIA `aria-labelledby` référençant un [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) identifié.",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut WAI ARIA `aria-label`.",
            "Une balise `<label>` ayant un attribut `for` est associée au [champ de formulaire](#champ-de-saisie-de-formulaire).",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut `title`.",
            "Un bouton adjacent au [champ de formulaire](#champ-de-saisie-de-formulaire) lui fournit une étiquette visible et un attribut WAI-ARIA `aria-label`, `aria-labelledby` ou `title` lui fournit un nom accessible."
          ],
          "2": [
            "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) associé à une balise `<label>` ayant un attribut `for`, vérifie-t-il ces conditions ?",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut `id`.",
            "La valeur de l'attribut `for` est égale à la valeur de l'attribut `id` du [champ de formulaire](#champ-de-saisie-de-formulaire) associé."
          ],
          "3": [
            "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) ayant une [étiquette](#etiquette-de-champ-de-formulaire) dont le contenu n'est pas visible ou à proximité (masqué, `aria-label`) ou qui n’est pas [accolé](#accoles-etiquette-et-champ-accoles) au champ (`aria-labelledby`), vérifie-t-il une de ses conditions ?",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut `title` dont le contenu permet de comprendre la nature de la saisie attendue.",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) est accompagné d'un [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) accolé au champ qui devient visible à la prise de focus permettant de comprendre la nature de la saisie attendue.",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) est accompagné d'un [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) visible accolé au champ permettant de comprendre la nature de la saisie attendue."
          ]
        },
        "references": [
          {
            "wcag": [
              "9.1.3.1 / 1.3.1 Info and Relationships (A)",
              "9.2.4.6 / 2.4.6 Headings and Labels (AA)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)",
              "9.4.1.2 / 4.1.2 Name, Role, Value (A)"
            ]
          },
          {
            "techniques": [
              "G82",
              "G131",
              "H44",
              "H65",
              "F68",
              "F82",
              "F86",
              "ARIA6",
              "ARIA9",
              "ARIA14",
              "ARIA16"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 2,
        "title": "Chaque [étiquette](#etiquette-de-champ-de-formulaire) associée à un [champ de formulaire](#champ-de-saisie-de-formulaire) est-elle pertinente (hors cas particuliers) ?",
        "tests": {
          "1": [
            "Chaque balise `<label>` permet-elle de connaître la fonction exacte du [champ de formulaire](#champ-de-saisie-de-formulaire) auquel elle est associée ?"
          ],
          "2": [
            "Chaque attribut `title` permet-il de connaître la fonction exacte du [champ de formulaire](#champ-de-saisie-de-formulaire) auquel il est associé ?"
          ],
          "3": [
            "Chaque étiquette implémentée via l'attribut WAI-ARIA `aria-label` permet-elle de connaître la fonction exacte du [champ de formulaire](#champ-de-saisie-de-formulaire) auquel elle est associée ?"
          ],
          "4": [
            "Chaque [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé via l'attribut WAI-ARIA `aria-labelledby` permet-il de connaître la fonction exacte du [champ de formulaire](#champ-de-saisie-de-formulaire) auquel il est associée ?"
          ],
          "5": [
            "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) ayant un [intitulé visible](#intitule-visible) vérifie-t-il ces conditions (hors cas particuliers) ?",
            "S'il est présent, le contenu de l'attribut WAI-ARIA `aria-label` du [champ de formulaire](#champ-de-saisie-de-formulaire) contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) lié au [champ de formulaire](#champ-de-saisie-de-formulaire) via un attribut WAI-ARIA `aria-labelledby` contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le contenu de l'attribut `title` du [champ de formulaire](#champ-de-saisie-de-formulaire) contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent le contenu de la balise `<label>` associé au [champ de formulaire](#champ-de-saisie-de-formulaire) contient au moins l'[intitulé visible](#intitule-visible)."
          ],
          "6": [
            "Chaque bouton adjacent au [champ de formulaire](#champ-de-saisie-de-formulaire) qui fournit une étiquette visible permet-il de connaître la fonction exacte du [champ de formulaire](#champ-de-saisie-de-formulaire) auquel il est associé ?"
          ]
        },
        "particularCases": [
          "Il existe une gestion de cas particulier pour le test 11.2.5 lorsque :",
          {
            "ul": [
              "La ponctuation et les lettres majuscules sont présentes dans le texte de l’[intitulé visible](#intitule-visible) : elles peuvent être ignorées dans le nom accessible sans porter à conséquence.",
              "Le texte de l’[intitulé visible](#intitule-visible) sert de symbole : le texte ne doit pas être interprété littéralement au niveau du nom accessible. Le nom doit exprimer la fonction véhiculée par le symbole (par exemple, \"B\" au niveau d'un éditeur de texte aura pour nom accessible \"Mettre en gras\", le signe \">\" en fonction du contexte signifiera \"Suivant\" ou \"Lancer la vidéo\"). Le cas des symboles mathématiques fait cependant exception (voir la note ci-dessous)."
            ]
          },
          "Note : si l'étiquette visible représente une expression mathématique, les symboles mathématiques peuvent être repris littéralement pour servir d'étiquette au nom accessible (ex. : \"A>B\"). Il est laissé à l'utilisateur le soin d'opérer la correspondance entre l'expression et ce qu'il doit épeler compte tenu de la connaissance qu'il a du fonctionnement de son logiciel de saisie vocale (\"A plus grand que B\" ou \"A supérieur à B\").",
          "Ce cas particulier s'applique également au test 11.9.3."
        ],
        "references": [
          {
            "wcag": [
              "9.2.4.6 / 2.4.6 Headings and Labels (AA)",
              "9.2.5.3 / 2.5.3 Label in Name (A)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "G82",
              "G131",
              "H44",
              "H65",
              "ARIA6",
              "ARIA9",
              "ARIA14",
              "ARIA16"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 3,
        "title": "Dans chaque formulaire, chaque [étiquette](#etiquette-de-champ-de-formulaire) associée à un [champ de formulaire](#champ-de-saisie-de-formulaire) ayant la même fonction et répété plusieurs fois dans une même page ou dans un [ensemble de pages](#ensemble-de-pages) est-elle [cohérente](#etiquettes-coherentes) ?",
        "tests": {
          "1": [
            "Chaque [étiquette](#etiquette-de-champ-de-formulaire) associée à un [champ de formulaire](#champ-de-saisie-de-formulaire) ayant la même fonction et répétée plusieurs fois dans une même page est-elle [cohérente](#etiquettes-coherentes) ?"
          ],
          "2": [
            "Chaque [étiquette](#etiquette-de-champ-de-formulaire) associée à un [champ de formulaire](#champ-de-saisie-de-formulaire) ayant la même fonction et répétée dans un ensemble de pages est-elle [cohérente](#etiquettes-coherentes) ?"
          ]
        },
        "references": [
          {
            "wcag": [
              "9.3.2.4 / 3.2.4 Consistent Identification (AA)"
            ]
          },
          {
            "techniques": [
              "F31"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 4,
        "title": "Dans chaque formulaire, chaque [étiquette de champ](#etiquette-de-champ-de-formulaire) et son champ associé sont-ils [accolés](#accoles-etiquette-et-champ-accoles) (hors cas particuliers) ?",
        "tests": {
          "1": [
            "Chaque [étiquette de champ](#etiquette-de-champ-de-formulaire) et son [champ](#champ-de-saisie-de-formulaire) associé sont-ils [accolés](#accoles-etiquette-et-champ-accoles) ?"
          ],
          "2": [
            "Chaque [étiquette](#etiquette-de-champ-de-formulaire) [accolée](#accoles-etiquette-et-champ-accoles) à un [champ](#champ-de-saisie-de-formulaire) (à l'exception des case à cocher, bouton radio ou balises ayant un attribut WAI-ARIA `role=\"checkbox\"`, `role=\"radio\"` ou `role=\"switch\"`), vérifie-t-elle ces conditions (hors cas particuliers) ?",
            "L'étiquette est visuellement [accolée](#accoles-etiquette-et-champ-accoles) immédiatement au-dessus ou à gauche du [champ de formulaire](#champ-de-saisie-de-formulaire) lorsque le sens de lecture de la langue de l'étiquette est de gauche à droite.",
            "L'étiquette est visuellement [accolée](#accoles-etiquette-et-champ-accoles) immédiatement au-dessus ou à droite du [champ de formulaire](#champ-de-saisie-de-formulaire) lorsque le sens de lecture de la langue de l'étiquette est de droite à gauche."
          ],
          "3": [
            "Chaque [étiquette](#etiquette-de-champ-de-formulaire) [accolée](#accoles-etiquette-et-champ-accoles) à un [champ](#champ-de-saisie-de-formulaire) de type `checkbox` ou `radio` ou à une balise ayant un attribut WAI-ARIA `role=\"checkbox\"`, `role=\"radio\"` ou `role=\"switch\"`, vérifie-t-elle ces conditions (hors cas particuliers) ?",
            "L'étiquette est visuellement [accolée](#accoles-etiquette-et-champ-accoles) immédiatement au-dessus ou à droite du [champ de formulaire](#champ-de-saisie-de-formulaire) lorsque le sens de lecture de la langue de l'étiquette est de gauche à droite.",
            "L'étiquette est visuellement [accolée](#accoles-etiquette-et-champ-accoles) immédiatement au-dessus ou à gauche du [champ de formulaire](#champ-de-saisie-de-formulaire) lorsque le sens de lecture de la langue de l'étiquette est de droite à gauche."
          ]
        },
        "particularCases": [
          "Les test 11.4.2 et 11.4.3 seront considérés comme non applicable :",
          {
            "ul": [
              "Dans le cas où l'[étiquette](#etiquette-de-champ-de-formulaire) mélange une portion de texte qui se lit de droite à gauche avec une portion de texte qui se lit de gauche à droite.",
              "Dans le cas où un formulaire contient des labels de plusieurs langues qui se liraient de droite à gauche et inversement. Par exemple un formulaire de commande en arabe qui propose une liste de cases à cocher de produit en langue française ou mixant des produits en langue arabe ou en langue française.",
              "Dans le cas où les champs de type `radio` ou `checkbox` et les balises ayant un attribut WAI-ARIA `role=\"checkbox\"`, `role=\"radio\"` ou `role=\"switch\"` ne sont pas visuellement présentés sous forme de bouton radio ou de case à cocher",
              "Dans le cas où les champs seraient utilisés dans un contexte où il pourrait être légitime, du point de vue de l'expérience utilisateur, de placer les étiquettes de manière différente à celle requise dans les tests 11.4.2 et 11.4.3."
            ]
          }
        ],
        "references": [
          {
            "wcag": [
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "G162"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 5,
        "title": "Dans chaque formulaire, les champs de même nature sont-ils regroupés, si nécessaire ?",
        "tests": {
          "1": [
            "Les champs de même nature vérifient-ils l'une de ces conditions, si nécessaire ?",
            "Les champs de même nature sont regroupés dans une balise <fieldset>.",
            "Les champs de même nature sont regroupés dans une balise possédant un attribut WAI-ARIA `role=\"group\"`.",
            "Les champs de même nature de type radio (<input type=\"radio\"> ou balises possédant un attribut WAI-ARIA role=\"radio\") sont regroupés dans une balise possédant un attribut WAI-ARIA role=\"radiogroup\" ou \"group\"."
          ]
        },
        "references": [
          {
            "wcag": [
              "9.1.3.1 / 1.3.1 Info and Relationships (A)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "H71",
              "ARIA17"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 6,
        "title": "Dans chaque formulaire, chaque regroupement de champs de formulaire a-t-il une [légende](#legende) ?",
        "tests": {
          "1": [
            "Chaque regroupement de champs de même nature possède-t-il une [légende](#legende) ?"
          ]
        },
        "references": [
          {
            "wcag": [
              "9.1.3.1 / 1.3.1 Info and Relationships (A)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "H71",
              "ARIA17"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 7,
        "title": "Dans chaque formulaire, chaque [légende](#legende) associée à un regroupement de champs de même nature est-elle pertinente ?",
        "tests": {
          "1": [
            "Chaque [légende](#legende) associée à un regroupement de champs de même nature est-elle pertinente ?"
          ]
        },
        "references": [
          {
            "wcag": [
              "9.1.3.1 / 1.3.1 Info and Relationships (A)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "H71",
              "ARIA17"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 8,
        "title": "Dans chaque formulaire, les items de même nature d'une liste de choix sont-ils regroupées de manière pertinente ?",
        "tests": {
          "1": [
            "Pour chaque balise `<select>`, les items de même nature d'une liste de choix sont-ils regroupés avec une balise <optgroup>, si nécessaire ?"
          ],
          "2": [
            "Dans chaque balise `<select>`, chaque balise <optgroup> possède-t-elle un attribut label ?"
          ],
          "3": [
            "Pour chaque balise `<optgroup>` ayant un attribut label, le contenu de l'attribut label est-il pertinent ?"
          ]
        },
        "technicalNote": [
          "Il est possible d’utiliser une balise ayant un attribut WAI-ARIA `role=\"listbox\"` en remplacement d’une balise `<select>`. En revanche, il est impossible de créer des groupes d’options via l’utilisation de WAI-ARIA. De ce fait, une liste nécessitant un regroupement d’options structurée à l’aide d’une balise ayant un attribut WAI-ARIA `role=\"listbox\"` sera considérée comme non conforme au [critère 11.8](#crit-11-8)."
        ],
        "references": [
          {
            "wcag": [
              "9.1.3.1 / 1.3.1 Info and Relationships (A)"
            ]
          },
          {
            "techniques": [
              "H85"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 9,
        "title": "Dans chaque formulaire, l'intitulé de chaque bouton est-il pertinent (hors cas particuliers) ?",
        "tests": {
          "1": [
            "L'intitulé de chaque bouton est-il pertinent ?",
            "S'il est présent, le contenu de l'attribut WAI-ARIA `aria-label` est pertinent.",
            "S'il est présent, le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) lié au bouton via un attribut WAI-ARIA `aria-labelledby` est pertinent.",
            "S'il est présent, le contenu de l'attribut value d'une balise `<input>` de type submit, reset ou button est pertinent.",
            "S'il est présent, le contenu de la balise `<button>` est pertinent.",
            "S'il est présent, le contenu de l'attribut alt d'une balise `<input>` de type image est pertinent.",
            "S'il est présent, le contenu de l'attribut title est pertinent."
          ],
          "2": [
            "Chaque bouton affichant un [intitulé visible](#intitule-visible) vérifie-t-il ces conditions (hors cas particuliers) ?",
            "S'il est présent, le contenu de l'attribut WAI-ARIA `aria-label` contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) lié au bouton via un attribut WAI-ARIA `aria-labelledby` contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le contenu de l'attribut value d'une balise `<input>` de type submit, reset ou button contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le contenu de la balise `<button>` contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le contenu de l'attribut alt d'une balise `<input>` de type image contient au moins l'[intitulé visible](#intitule-visible).",
            "S'il est présent, le contenu de l'attribut title contient au moins l'[intitulé visible](#intitule-visible)."
          ]
        },
        "particularCases": [
          "Pour le test 11.9.2, voir cas particuliers critère 11.2."
        ],
        "references": [
          {
            "wcag": [
              "9.2.5.3 / 2.5.3 Label in Name (A)",
              "9.4.1.2 / 4.1.2 Name, Role, Value (A)"
            ]
          },
          {
            "techniques": [
              "H36",
              "H91",
              "ARIA6",
              "ARIA9",
              "ARIA14",
              "ARIA16"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 10,
        "title": "Dans chaque formulaire, le contrôle de saisie est-il utilisé de manière pertinente (hors cas particuliers) ?",
        "tests": {
          "1": [
            "Les indications du caractère obligatoire de la saisie des champs vérifient-elles une de ces conditions (hors cas particuliers) ?",
            "Une indication de champ obligatoire est visible et permet d'identifier nommément le champ concerné préalablement à la validation du formulaire.",
            "Le champ obligatoire dispose de l'attribut aria-required=\"true\" ou required préalablement à la validation du formulaire."
          ],
          "2": [
            "Les champs obligatoires ayant l'attribut aria-required=\"true\" ou required vérifient-ils une de ces conditions ?",
            "Une indication de champ obligatoire est visible et située dans l'étiquette associé au champ préalablement à la validation du formulaire.",
            "Une indication de champ obligatoire est visible et située dans le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ préalablement à la validation du formulaire."
          ],
          "3": [
            "Les messages d'erreur indiquant l'absence de saisie d'un champ obligatoire vérifient-ils une de ces conditions ?",
            "le message d'erreur indiquant l'absence de saisie d'un champ obligatoire est visible et permet d'identifier nommément le champ concerné.",
            "Le champ obligatoire dispose de l'attribut aria-invalid=\"true\"."
          ],
          "4": [
            "Les champs obligatoires ayant l'attribut aria-invalid=\"true\" vérifient-ils une de ces conditions ?",
            "Une indication de champ obligatoire est visible et située dans l'étiquette associée au champ.",
            "Une indication de champ obligatoire est visible et située dans le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ."
          ],
          "5": [
            "Les instructions et indications du type de données et/ou de format obligatoires vérifient-elles une de ces conditions ?",
            "Une instruction ou une indication du type de données et/ou de format obligatoire est visible et permet d'identifier nommément le champ concerné préalablement à la validation du formulaire.",
            "Une instruction ou une indication du type de données et/ou de format obligatoire est visible dans l'étiquette ou le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ préalablement à la validation du formulaire."
          ],
          "6": [
            "Les messages d'erreurs fournissant une instruction ou une indication du type de données et/ou de format obligatoire des champs vérifient-ils une de ces conditions ?",
            "Le message d'erreur fournissant une instruction ou une indication du type de données et/ou de format obligatoires est visible et identifie le champ concerné.",
            "Le champ dispose de l'attribut aria-invalid=\"true\"."
          ],
          "7": [
            "Les champs ayant l'attribut aria-invalid=\"true\" dont la saisie requiert un type de données et/ou de format obligatoires vérifient-ils une de ces conditions ?",
            "Une instructions ou une indication du type de données et/ou de format obligatoire est visible et située dans la balise `<label>` associée au champ.",
            "Une instructions ou une indication du type de données et/ou de format obligatoire est visible et située dans le [passage de texte](#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ."
          ]
        },
        "particularCases": [
          "Le test 11.10.1 sera considéré comme non applicable lorsque le formulaire comporte un seul [champ de formulaire](#champ-de-saisie-de-formulaire) ou qu'il indique les champs optionnels de manière :",
          {
            "ul": [
              "Visible ;",
              "Dans la balise `<label>` ou dans la [légende](#legende) associée au champ."
            ]
          },
          "Dans le cas où l'ensemble des champs d'un formulaire sont obligatoires, le test 11.10.1 reste applicable."
        ],
        "references": [
          {
            "wcag": [
              "9.3.3.1 / 3.3.1 Error Identification (A)",
              "9.3.3.2 / 3.3.2 Labels or Instructions (AA)"
            ]
          },
          {
            "techniques": [
              "G83",
              "G84",
              "G85",
              "G89",
              "G184",
              "H44",
              "H81",
              "H89",
              "H90",
              "F81",
              "SCR18",
              "SCR32",
              "ARIA1",
              "ARIA2",
              "ARIA6",
              "ARIA9",
              "ARIA16",
              "ARIA21"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 11,
        "title": "Dans chaque formulaire, le contrôle de saisie est-il accompagné, si nécessaire, de suggestions facilitant la correction des erreurs de saisie ?",
        "tests": {
          "1": [
            "Pour chaque erreur de saisie, les types et les formats de données sont-ils suggérés, si nécessaire ?"
          ],
          "2": [
            "Pour chaque erreur de saisie, des exemples de valeurs attendues sont-ils suggérés, si nécessaire ?"
          ]
        },
        "technicalNote": [
          "Certains types de contrôles en HTML5 proposent des messages d'aide à la saisie automatique : par exemple le type email affiche un message du type « veuillez saisir une adresse e-mail valide » dans le cas où l'adresse e-mail saisie ne correspond pas au format attendu. Ces messages sont personnalisables via l'API Constraint Validation, ce qui permet de personnaliser les messages d'erreur et de valider le critère. L’attribut pattern permet d'effectuer automatiquement des contrôles de format (via des expressions régulières) et affiche un message d'aide personnalisable via l'attribut title : ce dispositif valide également le critère.",
          "Référence : <a href=\"https://www.w3.org/TR/html52/sec-forms.html#the-constraint-validation-api\">HTML 5.2 - 4.10.20.3 The constraint validation API</a>."
        ],
        "references": [
          {
            "wcag": [
              "9.3.3.3 / 3.3.3 Error Suggestion (AA)"
            ]
          },
          {
            "techniques": [
              "G84",
              "G85",
              "G89",
              "G177",
              "H89"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 12,
        "title": "Pour chaque formulaire qui modifie ou supprime des données, ou qui transmet des réponses à un test ou à un examen, ou dont la validation a des conséquences financières ou juridiques, la saisie des données vérifie-t-elle une de ces conditions ?",
        "tests": {
          "1": [
            "Pour chaque formulaire qui modifie ou supprime des données, ou qui transmet des réponses à un test ou un examen, ou dont la validation a des conséquences financières ou juridiques, la saisie des données vérifie-t-elle une de ces conditions ?",
            "L'utilisateur peut modifier ou annuler les données et les actions effectuées sur ces données après la validation du formulaire.",
            "L'utilisateur peut vérifier et corriger les données avant la validation d'un formulaire en plusieurs étapes.",
            "Un mécanisme de confirmation explicite, via une case à cocher (balise `<input>` de type checkbox ou balise ayant un attribut WAI-ARIA role=\"checkbox\") ou une étape supplémentaire, est présent."
          ],
          "2": [
            "Chaque formulaire dont la validation modifie ou supprime des données à caractère financier, juridique ou personnel vérifie-t-il une de ces conditions ?",
            "Un mécanisme permet de récupérer les données supprimées ou modifiées par l'utilisateur.",
            "Un mécanisme de demande de confirmation explicite de la suppression ou de la modification, via un [champ de formulaire](#champ-de-saisie-de-formulaire) ou une étape supplémentaire, est proposé."
          ]
        },
        "references": [
          {
            "wcag": [
              "9.3.3.4 / 3.3.4 Error Prevention (Legal, Financial, Data) (AA)"
            ]
          },
          {
            "techniques": [
              "G98",
              "G99",
              "G155",
              "G164",
              "G168"
            ]
          }
        ]
      }
    },
    {
      "criterium": {
        "number": 13,
        "title": "La finalité d'un champ de saisie peut-elle être déduite pour faciliter le remplissage automatique des champs avec les données de l'utilisateur ?",
        "tests": {
          "1": [
            "Chaque [champ de formulaire](#champ-de-saisie-de-formulaire) dont l'objet se rapporte à une information concernant l'utilisateur vérifie-t-il ces conditions ?",
            "Le [champ de formulaire](#champ-de-saisie-de-formulaire) possède un attribut autocomplete.",
            "L'attribut autocomplete est pourvu d'une valeur présente dans la liste des valeurs possibles pour l'attribut autocomplete associés à un [champ de formulaire](#champ-de-saisie-de-formulaire).",
            "La valeur indiquée pour l'attribut autocomplete est pertinente au regard du type d'information attendu."
          ]
        },
        "technicalNote": [
          "La liste des valeurs possibles pour l'attribut autocomplete repose sur la liste des valeurs présentes dans la spécifications WCAG2.1 qui reprend elle-même la liste des valeurs de type « field name » de la spécification HTML5.2. Le critère WCAG demande à ce que l'une de ces valeurs soit présente pour qualifier un champ de saisie concernant l'utilisateur.",
          "Ce que le critère WCAG laisse implicite, ce sont les différentes règles de construction possibles pour obtenir une valeur (simple ou composée) pour l'attribut autocomplete. C'est cependant l'affaire du développeur de fournir à l'attribut autocomplete une valeur ou un ensemble de valeurs valides au regard des exigences de l'algorithme fourni par la spécification HTML5.2. Ainsi, un attribut autocomplete ne peut contenir qu'une seule valeur de type field name, comme \"name\" ou \"street-address\". On peut avoir également un ensemble composé de différentes valeurs comme, par exemple, autocomplete=\"shipping name\" ou autocomplete=\"section-software shipping street-address\" : \"section-software\" renvoie à une valeur de type scope et \"shipping\" à une valeur de type hint set, mais toujours une seule valeur de type field name."
        ],
        "references": [
          {
            "wcag": [
              "9.1.3.5/ 1.3.5 Identify Input Purpose (AA)"
            ]
          },
          {
            "techniques": [
              "H98"
            ]
          }
        ]
      }
    }
  ]
}
