# Analyse thématique 10

## Entrées de glossaire =======================================================

`composant d interface`
`comprehensible ordre de lecture`
`contenu cache`
`contenu visible`
`contraste`
`feuille de style`
`indication donnee par la forme la taille ou la position`
`lien dont la nature n est pas evidente`
`media non temporel`
`media temporel type son video et synchronise`
`presentation de l information`
`prise de focus`
`site web ensemble de toutes les pages web`
`taille des caracteres`

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Compréhensible (ordre de lecture)`
* `Contenu visible`
* `Contraste`
* `Feuille de style`
* `Indication donnée par la forme, la taille ou la position`
* `Lien dont la nature n'est pas évidente`
* `Média non temporel` Modification syntaxique et de cohérence uniquement
* `Site web : ensemble de toutes les pages web`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Média temporel (type son, vidéo et synchronisé)` Modification mineure
* `Présentation de l'information`
* `Prise de focus`
* `Taille des caractères`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Contenu caché`

## Issues à mettre à jour dans Gitlab =========================================

* 10.2.1 => trop compliqué
* 10.3.1 => trop compliqué
* 10.4.2 => trop compliqué
* 10.4.3 => trop compliqué
* 10.5.* => à creuser
* 10.6.1 => à creuser
* 10.7.1 => conséquente. Chercher les entrées CSS de suppression de focus
* 10.8.1 => à creuser
* 10.9.* => trop compliqué
* 10.10.* => trop compliqué
* 10.11.* => trop compliqué
* 10.12.1 => à creuser. NMI : Chercher les modifications de `line-height`, `letter-spacing` et `word-spacing`, puis lever message
* 10.13.* => trop compliqué 
* 10.14.* => trop compliqué 
