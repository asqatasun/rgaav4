# Analyse thématique 01

## Glossaire ==================================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Alternative courte et concise` (différences syntaxiques uniquement)
* `CAPTCHA`
* `Correctement restitué (par les technologies d'assistance)`
* `Image objet`
* `Image réactive`
* `Image-test`
* `Image texte`
* `Mécanisme de remplacement`
* `Texte stylé`
* `URL`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Alternative textuelle (image)` Modification majeure (nombreuses et en profondeur)
* `Bouton (formulaire)` Modification majeure
* `Description détaillée (image)`
* `Image de décoration`
* `Image porteuse d'information`
* `Lien adjacent` --> `Lien ou bouton adjacent`
* `Zone (d'une image réactive)` modification mineure
* `Zone non cliquable`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Contenu alternatif`
* `Légende`
* `Passage de texte lié par aria-labelledby ou aria-describedby`

## Cas particuliers ===========================================================

### Cas particuliers identiques

* 1.3

### Cas particuliers supprimés

* 1.9

### Cas particuliers modifiés

* 1.8

## Notes techniques ===========================================================

### Notes techniques supprimés

* 1.1
* 1.3
* 1.10

### Notes techniques identiques

* 1.8

### Notes techniques modifiés

* 1.2
* 1.6

### Notes techniques ajoutées

* 1.9

## Issues à mettre à jour dans Gitlab =========================================

## Remarques globales à la thématique 1

* Si l'image est le seul élément d'un lien, son alternative doit être évaluée par la thématique 6 (liens)
* L'entrée de glossaire [Alternative textuelle (image)](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image) 
  décrit de manière **précise et exhaustive** :
    * ce qui doit être considéré comme une image
    * l'algorithme à appliquer pour obtenir l'alternative d'une image

## Retours spécifiques

### 1.1.x

=> conséquente

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4
* remonter l'alternative, et faire une NMI

### 1.2.x

=> conséquente

* L'entrée de glossaire [image de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration) précise qu'une image avec WAI-ARIA `role="img"` ne
 peut être considéré comme image de déco que si elle possède **aussi** `aria-hidden="true"`
* L'entrée de glossaire [Légende d'image](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende-d-image) décrit **précisement** la gestion à faire de `figure` et
 `figcaption`
* Reprendre ce qui a été fait en RGAAv3 et adapter au v4 

### 1.3.1 -> 1.3.7

=> conséquente

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4
* Version ++ : ajouter la gestion des marqueurs pour les captchas  

### 1.3.8

=> trop compliqué

### 1.3.9

=> conséquente 

* remonter l'alternative, si longueur >80 caractères, alors NMI + message, sinon passed (yes !)

### 1.4.x

=> conséquente 

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4
* Version ++ : ajouter la gestion des marqueurs pour les captchas  

### 1.5.x

=> conséquente

* Version ++ : ajouter la gestion des marqueurs pour les captchas

### 1.6.1 -> 1.6.7

=> conséquente 

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4

### 1.6.8

=> trop compliqué

### 1.6.9 + 1.6.10

=> à creuser

### 1.7.x

=> conséquente 

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4

### 1.8.x

=> conséquente 

* Reprendre ce qui a été fait en RGAAv3 et adapter au v4

### 1.9.x

=> à creuser
