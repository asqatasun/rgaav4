# Analyse thématique 12

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Compréhensible (ordre de lecture)`
* `Moteur de recherche (interne à un site web)`
* `Ordre de tabulation`
* `Page « plan du site »`
* `Script`
* `Système de navigation`
* `Zone d'en-tête`
* `Zone de contenu principal`
* `Zone de pied de page`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Ensemble de pages` Une modification mineure (a priori sans impact sur les règles)
* `Liens d'évitement ou d'accès rapide`
* `Barre de navigation` --> `Menu et barre de navigation` Modifications mineures
* `Prise de focus`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Landmarks`
* `Raccourci clavier`

## Issues à mettre à jour dans Gitlab =========================================

### 12.2.1

* RGAAv3 12.2.1 + RGAAv3 12.2.2 sont fusionnés dans RGAAv4 12.2.1
* reprendre les règles RGAAv3 existantes et fusionner

=> conséquente

### 12.3.1 + 12.3.3

* faire une NMI
* chercher dans la page les liens "plan du site"
* créer une nomenclature pour les variations dans la même langue et dans plusieurs langues (sitemap...)
* Attraper intitulé + cible du lien plan du site, c'est ce qu'il faut remonter à l'utilisateur et lui demander de vérifier

=> conséquente

### 12.3.2

* chercher dans la page les liens "plan du site"
* créer une nomenclature pour les variations dans la même langue et dans plusieurs langues (sitemap...)
* charger la page plan du site:
    * si code retour HTTP est en 400 ou 500 alors FAIL
    * sinon PASSED
* MF: preneur d'avis critique

=> conséquente

### 12.6.1 + 12.7.* + 12.10

=> à creuser

### 12.8.* + 12.9 + 12.11

=> trop compliqué


