# Analyse thématique 06

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Ancre` (différences syntaxiques uniquement)

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Contexte du lien` 
* `Intitulé de lien` --> `Intitulé (ou nom accessible) de lien"` Modification majeure
* `Lien` Modification majeure
* `Lien composite`
* `Lien image` Modification majeure
* `Lien texte` Modification majeure
* `Lien vectoriel` --> `Lien SVG` 

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Intitulé visible`

## Issues à mettre à jour dans Gitlab =========================================

## Remarques sur le *critère* 6.1 

La thématique 6 a été sensiblement remaniée.
En effet, le concept de *nom accessible* a été introduit en WCAG 2.1 / RGAAv4 et modifie les traitements.
Il convient d'être vigilant quant au vocabulaire utilisé (et donc au nommage des objets dans le code), par exemple
*intitulé de lien* (*link text* en anglais) et *nom accessible* (*accessible name* en anglais) sont deux
concepts bien distincts.  

Les entrées de glossaire : 

* [Intitulé (ou nom accessible) de lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
* [Contexte du lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contexte-du-lien)
* [Lien texte ](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-texte)
* [Lien image](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-image)
* [Lien composite](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-composite)
* [Lien SVG](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-svg)
* [Lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien)

...décrivent de manière **très précise et exhaustive** quels objets techniques manipuler. C'est une véritable avancée par rapport au RGAAv3 et à AccessiWeb 2.2.
Ces précisions rendent l'implémentation de nos règles beaucoup plus cadrée.

Le critère RGAAv3 6.2 est désormais intégré dans le critère RGAAv4 6.1. 
Ce sont les notes 1 et 2 de l'entrée de glossaire
[Intitulé (ou nom accessible) de lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
qui précisent ce fait. 

Les critères RGAAv3 6.3 et 6.4 sont désormais intégrés dans le critère RGAAv4 6.1.
C'est la note 3 de l'entrée de glossaire
[Intitulé (ou nom accessible) de lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien)
qui précise ce fait.

Mécaniquement, les tests 6.1.* deviennent plus conséquents.  

* 6.1.* => conséquente
* 6.2.1 => conséquente
