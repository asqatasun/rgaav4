# Analyse thématique 04

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `CAPTCHA`
* `Compatible avec les technologies d'assistance`
* `Contrôle (son déclenché automatiquement)`
* `Environnement maîtrisé`
* `Média non temporel` Modification syntaxique et de cohérence uniquement
* `URL`
* `Version alternative « audio seulement »`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Accessible et activable par le clavier et la souris` --> `Accessible et activable par le clavier et tout dispositif de pointage`
* `Audiodescription synchronisée (média temporel)` Modification mineure
* `Contrôle de la consultation (d'un média temporel)`
* `Lien adjacent` --> `Lien ou bouton adjacent`
* `Média temporel (type son, vidéo et synchronisé)` Modification mineure
* `Sous-titres synchronisés (objet multimédia)` Modification mineure (a priori sans impact pour nous)
* `Transcription textuelle (média temporel)`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Contenu alternatif`

## Issues à mettre à jour dans Gitlab =========================================

### 4.1 + 4.2 + 4.3.1 + 4.4.1 + 4.5 + 4.6 + 4.7 + 4.11

* semi-décidable
* Détection de média temporel : chercher les balises `video`, `audio`, `svg`, `canvas`, `bgsound`
* renvoyer NMI (trouver le message)

=> quickwin

### 4.3.2 

* décidable
* chercher les balises `video`, `audio`, `svg`, `canvas`, `bgsound`, avec balise fille `track`, vérifier sir présence de `kind="captions"`
* si oui conforme, sinon non-conforme

=> quickwin 

## 4.8 + 4.9 + 4.12

* semi-décidable
* Détection de média NON temporel : chercher les balises `svg`, `canvas`
* renvoyer NMI (trouver le message)

=> quickwin 

## 4.10.1

* semi-décidable
* Détection de son : chercher les balises `video`, `audio`, `object`, `embed`, `bgsound`
* renvoyer NMI (trouver le message)

=> quickwin 

## 4.13

* semi-décidable
* Détection : chercher les balises `video`, `audio`, `svg`, `canvas`, `bgsound`
* renvoyer NMI (trouver le message)

=> quickwin 
