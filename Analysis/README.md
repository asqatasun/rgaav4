# README.md

## Méthodologie d'analyse

Travail à faire pour chaque thématique :

### 0. Restreindre le fichier RGAAv4 à la thématique de travail

Par exemple pour travailler sur la thématique 01:

```shell script
jq '.topics[0]' RGAAv4/criteres.json > Analysis/RGAAv4_criteres_thematique01.json
```

Pour la thématique 02 :

```shell script
jq '.topics[1]' RGAAv4/criteres.json > Analysis/RGAAv4_criteres_thematique02.json
```

### 1. Lister les liens vers glossaire, notes techniques, cas particuliers

```shell script
grep -o -P "\[.*?\]\(.*?\)" Analysis/RGAAv4_criteres_thematique01.json \
  | perl -p -e 's/\[.*?\]\(\#(.*?)\)/\`$1\`/' \
  | grep -v "crit-" \
  | perl -p -e 's/-/ /g' \
  | sort \
  | uniq
```

### 2. Tri des entrées glossaire

Répartir les entrées de glossaire de la thématique selon les catégories suivantes :

* Entrées de glossaire RGAA v3/v4 : identiques
* Entrées de glossaire RGAA v3/v4 : supprimées
* Entrées de glossaire RGAA v3/v4 : ajoutées
* Entrées de glossaire RGAA v3/v4 : modifiées

### Report des Cas particuliers & Notes techniques

* copier / coller les Cas particuliers & Notes techniques de la thématique dans le fichier `Analyse_thematique_XX.md`

