# Analyse thématique 05

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Tableau de mise en forme`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `en tete de colonne ou de ligne` Modification mineure
* `Résumé (de tableau)`
* `Tableau de données`
* `Tableau de données complexe`
* `Titre d'un tableau (de données)` --> `Tableau de données ayant un titre`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `passage de texte associe au tableau de donnees`

## Issues à mettre à jour dans Gitlab =========================================

### TEST - trop compliqué


### TEST - à creuser

* 5.6.1
* 5.6.2
* 5.6.3
  
### Règle ITQ


### Règle IAR


### Règle QuickWin


### Règle Détection (NA)


### Règle PréQualifié (NMI)

* 5.7.1
* 5.7.2
* 5.7.3
* 5.7.4
* 5.7.5

### Règle Conséquente

#### 5.1.1

* Passe 1
    * Sélection 1 :
        * si doctype=HTML4 : balise `table` ou balise portant l'attribut `role="table"`, avec un attribut `summary`
        * si doctype=HTML5 : balise `table` ou balise portant l'attribut `role="table"`, avec une balise `caption`  
    * Traitement 1 : 
        * Résultat : NMI / à priori conforme
        * Message : Vérifier que tableau = tableau de données complexe 
* Passe 2
    * Sélection 2 :
        * si doctype=HTML4 : balise `table` ou balise portant l'attribut `role="table"`, **SANS** un attribut `summary`
        * si doctype=HTML5 : balise `table` ou balise portant l'attribut `role="table"`, **SANS** une balise `caption`  
    * Traitement 2 : 
        * Résultat : NMI 
        * Message : Si tableau de données complexe, vérifier présence de [résumé de tableau](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#resume-de-tableau)

#### 5.4.1

* Passe 1
    * Sélection 1 :
        * balise `table` ou balise portant l'attribut `role="table"`, avec une balise `caption`
        * balise `table` ou balise portant l'attribut `role="table"`, avec un attribut `title`
        * balise `table` ou balise portant l'attribut `role="table"`, avec un attribut `aria-labelledby`
        * balise portant l'attribut `role="table"` et l'attribut `aria-label` (question : est-ce qu'une balise fille portant un `aria-label` est recevable ?)
    * Traitement 1 : 
        * Résultat : NMI / à priori conforme
        * Message : Vérifier que tableau = tableau de données 
* Passe 2
    * Sélection 2 :
        * balise `table` ou balise portant l'attribut `role="table"`, **SANS** une balise `caption`
        * balise `table` ou balise portant l'attribut `role="table"`, **SANS** un attribut `title`
        * balise `table` ou balise portant l'attribut `role="table"`, **SANS** un attribut `aria-labelledby`
        * balise portant l'attribut `role="table"` et **SANS** l'attribut `aria-label` (question : est-ce qu'une balise fille portant un `aria-label` est recevable ?)
    * Traitement 2 :
        * Résultat : NMI
        * Message : Si tableau = tableau de données, chercher son titre    
     
Ressources : 

* https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/Table_Role
* https://www.w3.org/TR/wai-aria-practices/examples/table/table.html

#### 5.5.1

* Sélection 1 :
    * balise `table` ou balise portant l'attribut `role="table"`, avec une balise `caption`
    * balise `table` ou balise portant l'attribut `role="table"`, avec un attribut `title`
    * balise `table` ou balise portant l'attribut `role="table"`, avec un attribut `aria-labelledby`
    * balise portant l'attribut `role="table"` et l'attribut `aria-label` (question : est-ce qu'une balise fille portant un `aria-label` est recevable ?)
* Traitement 1 : 
    * Résultat : NMI
    * Message : vérifier pertinence du titre 
