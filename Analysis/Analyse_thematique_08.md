# Analyse thématique 08

## Issues à mettre à jour dans Gitlab

### TEST - trop compliqué


### TEST - à creuser

* 8.10.1 Faire une règle NMI en cherchant du texte écrit en arabe (dans le cas langue par défaut = LTR).
  Lister les langues RTL autres que l'arabe. Ensuite conder la recherche dans "le sens contraire" 
  (i.e langue par défaut est RTL)
  
### Règle ITQ


### Règle IAR


### Règle QuickWin

* 8.8.1 (= fusion de RGAAv3 8.8.1 + RGAAv3 8.8.2)

### Règle Détection (NA)


### Règle PréQualifié (NMI)

* 8.10.2 Chercher les attribut `dir`

### Règle Conséquente

* 8.2.1 brancher le validateur W3C
