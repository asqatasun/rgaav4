# Analyse thématique 09

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Zone d'en-tête`
* `Zone de contenu principal`
* `Zone de pied de page`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Listes` Modification majeure
* `Barre de navigation` --> `Menu et barre de navigation` Modifications mineures
* `Titre`

### Entrées de glossaire RGAA v3/v4 : ajoutées


## Issues à mettre à jour dans Gitlab =========================================

### TEST - trop compliqué

* 9.1.3 : aucune idée de quoi implémenter
* 9.4.* : A priori rien à implémenter (MF). Un autre cerveau peut-il vérifier ?

### TEST - à creuser

* 9.3.* : A priori rien à implémenter (MF). Un autre cerveau peut-il vérifier ?
  
### Règle ITQ


### Règle IAR


### Règle QuickWin

#### 9.1.1

* Note 1 : attention ajout ARIA
* Note 2 : **attention** une hiérarchie commençant par autre chose qu'un H1 est **valide**

#### 9.1.2

* Note : attention ajout ARIA

### Règle Détection (NA)


### Règle PréQualifié (NMI)


### Règle Conséquente

#### 9.2.1

* Si HTML<5 Alors NA
* Si plusieurs `main` alors FAIL (la cas d'un CSS masquant les autres `main` est volontairement ignoré car si CSS est désactivé le test est mécaniquement invalidé, gnarck, gnarck, gnarck :) )
* Si absence de `header` alors FAIL
* Si absence de `nav` alors FAIL
* Si absence de `main` alors FAIL
* Si absence de `footer` alors FAIL
* Si :
    * au moins un moins un `header` ET
    * au moins un moins un `nav` ET
    * au moins un moins un `footer` ET
    * un `main`,
    * ALORS résultat = NMI, message = vérifier que ... (reprendre intitulé du test)

## Autres

* 9.1.1 supprimé
* RGAAv3 9.1.2 devient RGAAv4 9.1.1
* RGAAv3 9.1.3 devient RGAAv4 9.1.3 mais est modifié profondément 
* RGAAv3 9.1.4 devient RGAAv4 9.1.2 avec modification (ajout ARIA) 
