# Analyse thématique 03

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Contraste`
* `Image véhiculant une information (donnée par la couleur)`
* `Média non temporel` Modification syntaxique et de cohérence uniquement
* `Propriété CSS déterminant une couleur`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Image porteuse d'information`
* `Information (donnée par la couleur)`
* `Média temporel (type son, vidéo et synchronisé)` Modification mineure

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Couleur d'arrière-plan contiguë et couleur contiguë`
* `Mécanisme qui permet d'afficher un rapport de contraste conforme`

## Issues à mettre à jour dans Gitlab =========================================

* 3.1.1 trop compliqué
* 3.1.2 à creuser : chercher les astérisques décorées CSS dans un formulaire, lever NMI suspected fail
* 3.1.3 trop compliqué
* 3.1.4 Détecter en CSS `color`, `background-color`, `background`, `border-color`, `border`, `outline-color`, `outline`, `background:url`…. Renvoyer NMI
* 3.1.5 à creuser : Voir après avoir fait la Th4 si possible de détecter un média temporel
* 3.1.6 à creuser : Voir après avoir fait la Th4 si possible de détecter un média non temporel

et 

* 3.2.5 à creuser
* 3.3.* à creuser

### 3.2.1

* Reprendre la règle RGAAv3 3.3.1.
* L'algo est le même **SAUF** sur la taille des caractères : 
    * `**jusqu'à 150%** de la taille de police par défaut (ou 1.5em)` devient 
    * `d'une taille restituée inférieure à 24px`

=> quickwin

### 3.2.2

* Reprendre la règle RGAAv3 3.3.2.
* L'algo est le même **SAUF** sur la taille des caractères : 
    * `**jusqu'à 120%** de la taille de police par défaut (ou 1.2em)` devient 
    * `d'une taille restituée inférieure à 18,5px`

=> quickwin

### 3.2.3

* Reprendre la règle RGAAv3 3.3.3. 
* L'algo est le même **SAUF** sur la taille des caractères : 
    * `**jusqu'à 150%** de la taille de police par défaut (ou 1.5em)` devient 
    * `d'une taille restituée inférieure à 24px`

=> quickwin

### 3.2.4

* Reprendre la règle RGAAv3 3.3.4.
* L'algo est le même **SAUF** sur la taille des caractères : 
    * `**jusqu'à 120%** de la taille de police par défaut (ou 1.2em)` devient 
    * `d'une taille restituée inférieure à 18,5px`


=> quickwin




## Autres informations

* Renumérotation + modification : RGAAv3 3.3 devient RGAAv4 3.2
* Contrairement à ce qu'on pourrait imaginer de prime abord, le RGAAv4 3.3 n'est **pas** une renumérotation du RGAAv3 3.4.
  Il s'agit bien d'un nouveau test portant sur le nouveau concept de couleur contigue.

