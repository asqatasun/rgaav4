# Analyse thématique 07

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Alternative (à script)` (différences syntaxiques uniquement)
* `Changement de contexte`
* `Compatible avec les technologies d'assistance`
* `Correctement restitué (par les technologies d'assistance)`
* `Script`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Accessible et activable par le clavier et la souris` --> `Accessible et activable par le clavier et tout dispositif de pointage` => négligeable
* `Cadre en ligne` --> `Cadre`
* `Le nom, le rôle, la valeur, le paramétrage et les changements d'états`
* `Message de statut`

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Intitulé visible`

## Issues à mettre à jour dans Gitlab =========================================

### 7.1.*

* faire une règle de détection (NA) : balise `script`, liste des "attributs JS" en HTML (chercher sur MDN) et toute autre manière de charger du JS 

=> Quickwin

### 7.2.*

=> Quickwin

* faire une règle de sélection (NMI) en cherchant les balises `script` et `noscript`

### 7.3.1

* Traquer tous les attributs relatifs à la souris (ex `onmouseover`) ou au clavier (ex `onkeypress`), et établir la correspondance (ex: `onclick` à la souris équivaut à `onkeypress` au clavier)
* Pour un de ces attributs, vérifier que la balise porte aussi l'attribut correspondant
* Sinon fail :)
* Aller chercher dans MDN la liste des attributs souris et clavier

=> conséquente

### 7.4.1

=> ITQ

### 7.5.1

* Chercher les `role="status"`. Renvoyer NMI (trouver le bon message).

=> conséquente

### 7.5.2

* Chercher les `role="alert"`. Renvoyer NMI (trouver le bon message).

=> conséquente

### 7.5.3

* Chercher les `role="log"`, `role="progressbar"`, `role="status"`. Renvoyer NMI (trouver le bon message).
* Comme le suggère la Note Technique, enrichir en cherchant aussi les combinaisons suivantes :
    * `aria-live="polite"`
    * `aria-live="polite"` et un attribut `aria-atomic="true"`
    * `aria-live="assertive"` et un attribut `aria-atomic="true"`

=> conséquente



