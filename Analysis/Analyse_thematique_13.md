# Analyse thématique 13

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 

* `Changement brusque de luminosité ou effet de flash`
* `Contrôle (contenu en mouvement ou clignotant)`
* `Procédé de rafraîchissement`
* `Script`
* `URL`
* `Version accessible (pour un document en téléchargement)`

### Entrées de glossaire RGAA v3/v4 : modifiées

* `Redirection automatique` --> `Redirection` Modification mineure

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Composant d'interface`
* `Gestes complexes et gestes simples`
* `Pressé ou posé`
* `Relâché ou relevé`

## Issues à mettre à jour dans Gitlab =========================================

### 13.1.1

=> à creuser

### 13.1.2

=> quickwin

### 13.1.3 + 13.1.4 

=> trop compliqué

### 13.2.1 (RGAAv3 : 13.2.x)

=> quickwin 

### 13.3.1 (RGAAv3 : 13.7.x)

=> quickwin

### 13.4.1 + 13.5.1 + 13.6.1 + 13.7.x + 13.8.x + 13.9.1 + 13.10.x + 13.11.1 + 13.12.x

=> trop compliqué



