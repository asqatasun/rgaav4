# Analyse thématique 11

## Entrées de glossaire =======================================================

### Entrées de glossaire RGAA v3/v4 : identiques 


### Entrées de glossaire RGAA v3/v4 : modifiées

* `Champ de saisie de formulaire`
* `Ensemble de pages` Une modification mineure (a priori sans impact sur les règles)
* `Étiquette de champ de formulaire` Modification majeure

### Entrées de glossaire RGAA v3/v4 : ajoutées

* `Accolés (étiquette et champ accolés)`
* `Étiquettes cohérentes`
* `Intitulé visible`
* `Légende`
* `Passage de texte lié par aria-labelledby ou aria-describedby`

## Issues à mettre à jour dans Gitlab =========================================

### Remarques globales

L'entrée de glossaire [Champ de saisie de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire)
liste de manière **très précise et exhaustive** les objets à manipuler pour les champs de formulaire. :warning: La reprise des règles existantes RGAAv3 demande mécaniquement une revue sous cet aspect.

### 11.1.1 + 11.1.2 + 11.3.1 + 11.3.2 

Reprendre la règle RGAAv3 associée et la refaire au regard de l'entrée de glossaire 
[Champ de saisie de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) 

Cette dernière liste de manière **très précise et exhaustive** les objets à manipuler pour les champs de formulaire (véritable avancée par rapport à RGAAv3 ou AW22)

=> conséquente

### 11.1.3

=> à creuser

### 11.2.1 + 11.2.2 + 11.2.3 + 11.2.4  

* reprendre ce qui a été fait pour RGAAv3 11.2.1

=> quickwin

### 11.2.5 + 11.2.6

=> à creuser

### 11.4.1 + 11.4.2 + 11.4.3

* reprendre la règle RGAAv3 11.4.1 et l'adapter

=> conséquente

### 11.5.1 

* reprendre la règle RGAAv3 11.5.1 et l'adapter

=> conséquente

### 11.6.1

* reprendre la règle RGAAv3 11.5.1 et l'adapter
* :warning: l'entrée de glossaire [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) ajoute de l'ARIA

=> conséquente

### 11.7.1

* reprendre la règle RGAAv3 11.7.1 et l'adapter
* :warning: l'entrée de glossaire [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) ajoute de l'ARIA

=> conséquente

### 11.8.*

* reprendre la règle RGAAv3 11.8.* et l'adapter
* :warning: la [note technique du 11.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-11-8) ajouter un cas d'invalidation avec de l'ARIA 

=> conséquente

### 11.9.1

* reprendre la règle RGAAv3 11.9.1 et l'adapter

=> conséquente

### 11.9.2

==> à creuser

### 11.10.*

==> à creuser

### 11.11.*

* reprendre la règle RGAAv3 11.11.*
* vérifier si la note technique a une implication sur la règle

=> conséquente

## 11.12*

* reprendre la règle RGAAv3 11.12.*

=> quickwin

## 11.13.1

=> à creuser
