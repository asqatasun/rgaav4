<?php
declare(strict_types = 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
use League\HTMLToMarkdown\HtmlConverter;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use function Symfony\Component\String\u; // the u() function creates Unicode strings

// Config
$builidPath   = 'build/'.date('Y-m-d_H\hi');
$srcPath      = "$builidPath/source";
$dataPath     = "$builidPath/data";
$srcFile  = 'rule-rgaa30-I18N_fr.properties';
$srcUrl   = 'https://raw.githubusercontent.com/Asqatasun/Asqatasun/v4.1.0/rules/rules-rgaa3.0/src/main/resources/i18n/rule-rgaa30-I18N_fr.properties';

// Intialize tools
$filesystem = new Filesystem();
$httpClient = HttpClient::create();
$converter = new HtmlConverter();

// Clean up directories
$filesystem->remove("$builidPath");

// Retrieve source file
$response = $httpClient->request('GET', "$srcUrl");
if($response->getStatusCode() !== 200){
    throw new Exception("URL [ $srcUrl ] is not available");
}
$srcData = $response->getContent();
$filesystem->dumpFile("$srcPath/$srcFile", "$srcData");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Processing
$rulesNumber = 0;
$data = [];
$srcLines = file("$srcPath/$srcFile");
foreach ($srcLines as $line) {
    $line = u($line)->collapseWhitespace()->trim()->trim("\\")->toString();
    if(preg_match("/^Rgaa30-(\d+)-(\d+)-(\d+)=/", $line, $matches)) {
        if (isset($fullId)) {
            $html = $data[$topicId][$criterionId][$testId]['html'];
            $markdown = $converter->convert($data[$topicId][$criterionId][$testId]['html']);
            $data[$topicId][$criterionId][$testId]['markdown'] = u($markdown)->trim()->toString();
            $data[$topicId][$criterionId][$testId]['html'] = u($html)->trim()->toString();
        }
        $rulesNumber++;
        $topicId = $matches[1];
        $criterionId = $matches[2];
        $testId = $matches[3];
        $fullId = "$topicId.$criterionId.$testId";
        $data[$topicId][$criterionId][$testId]['id'] = $fullId;
        $data[$topicId][$criterionId][$testId]['html'] = '';
        $data[$topicId][$criterionId][$testId]['markdown'] = '';
        $line = u($line)->replace($matches[0], '')->toString();
        echo "----> $fullId\n";
    }

    if (!preg_match("/^Rgaa30-(\d+)-(\d+)-(\d+)-url=/", $line) && $rulesNumber > 0) {
        $data[$topicId][$criterionId][$testId]['html'] .= "$line\n";
        echo "$line\n";
    }
}
$html = $data[$topicId][$criterionId][$testId]['html'];
$markdown = $converter->convert($data[$topicId][$criterionId][$testId]['html']);
$data[$topicId][$criterionId][$testId]['markdown'] = u($markdown)->trim()->toString();
$data[$topicId][$criterionId][$testId]['html'] = u($html)->trim()->toString();


$topics = [
    1 => 'Images',
    2 => 'Cadres',
    3 => 'Couleurs',
    4 => 'Multimedia',
    5 => 'Tableaux',
    6 => 'Liens',
    7 => 'Scripts',
    8 => 'Éléments obligatoires',
    9 => 'Structuration de l’information',
    10 => 'Présentation de l’information',
    11 => 'Formulaires',
    12 => 'Navigation',
    13 => 'Consultation',
];

// Create Json file
$json = '';
$topicCount = count($data);
foreach($data as $topicId => $topicData) {
    $topicName =  $topics[$topicId];
    $json .= "    {\n";
    $json .= "      \"topic\": \"$topicName\",\n";
    $json .= "      \"number\": $topicId,\n";
    $json .= "      \"criteria\": [\n";
    $criteraCount = count($topicData);
    foreach($topicData as $criterionId => $criterionData) {
        $json .= "        {\n";
        $json .= "          \"criterium\": {\n";
        $json .= "            \"number\": $criterionId,\n";
        $json .= "            \"tests\": {\n";
        $testsCount = count($criterionData);
        foreach($criterionData as $testId => $testData) {
            $json .= "              \"$testId\": [\n";
            $testDataLines = explode ("\n", $testData['markdown']);
            $testLinesCount = count($testDataLines);
            foreach ($testDataLines as $testLineNumber => $testLine) {
                $testLine = u($testLine)->trim()->toString();
                if(!empty($testLine)) {
                    $testLine = u($testLine)->replace('"', '\"')->toString();
                    if(substr($testLine, 0, 2) === '- '){
                        $testLine = substr($testLine, 2);
                    }
                    $endOfTestLine = ',';
                    if($testLineNumber+1 === $testLinesCount){
                        $endOfTestLine = '';
                    }
                    $json .= "                \"$testLine\"$endOfTestLine\n";
                }
            }
            $endOfTest = ',';
            if($testId === $testsCount){
                $endOfTest = '';
            }
            $json .= "              ]$endOfTest\n";
        }

        $endOfCriteria = ',';
        if($criterionId === $criteraCount){
            $endOfCriteria = '';
        }
        $json .= "            }\n";
        $json .= "          }\n";
        $json .= "        }$endOfCriteria\n";
    }
    $endOfTopic  = ',';
    if($topicId === $topicCount){
        $endOfTopic = '';
    }
    $json .= "      ]\n";
    $json .= "    }$endOfTopic\n";
}

$json =  u($json)->trim()->toString();
$jsonGlobal = ''.
"{
  \"wcag\": {
    \"version\": 2.1
  },
  \"topics\": [
$json
  ]
}";

echo "\n$jsonGlobal\n";
echo "\n\n$rulesNumber\n";
$filesystem->dumpFile("$dataPath/RGAA.3_tests.json", $jsonGlobal );

// Backup files
$filesystem->remove("./data");
$filesystem->remove("./source");
$filesystem->mirror($dataPath, "./data");
$filesystem->mirror($srcPath, "./source");
