# Import RGAA tests as Gitlab issues

## Configure


```bash
cp .env.dist .env
vim .env
```

```dotenv
# Paths
DATA_PATH="../RGAA.4_extraction/data"
REFERENCIAL_PATH="${DATA_PATH}/referential"
REFERENCIAL_EASY_PATH="${DATA_PATH}/referential_easy-path"
TEST_ID_FILE_PATH="${REFERENCIAL_PATH}/tests_IDs.txt"
CRITERIA_FILE_WITH_PARTICULAR_CASES="${REFERENCIAL_PATH}/criteria_IDs_withParticularCases.txt"
CRITERIA_FILE_WITH_TECH_NOTE="${REFERENCIAL_PATH}/criteria_IDs_withTechnicalNote.txt"

# GITLAB configuration
GITLAB_URL="https://gitlab.example.com"
GITLAB_PRIVATE_TOKEN="<yourGitlabPrivateToken>"
GITLAB_PROJECT_ID="<yourGitLabProject_ID>" 
```

### Gitlab documentation
- [Creating a personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
- [Limiting scopes of a personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token)


## Run

```bash
composer install
php 99_import-rgaa-tests_as-GitLab-issues.php
```

