<?php
/**
 * ----------------------------------
 * Import RGAA tests as Gitlab issues
 * ----------------------------------
 * configure :  $gitlaUrl
 *              $gitlabPrivateToken
 *              $gitlaProjectId
 * ----------------------------------
 * @todo check ID test format
 * @todo add recovery management if API requests fail
 * ----------------------------------
 */
declare(strict_types=1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Filesystem\Filesystem;

// Intialize tools
$filesystem = new Filesystem();

// Get configuration variables
$envFile = __DIR__ . '/.env';
if (!$filesystem->exists("$envFile") || !is_readable("$envFile")) {
    throw new Exception("[ /.env ] file is not available. See example file : [ /.env.dist ]");
}
$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/.env');
$dataPath = $_ENV['DATA_PATH'];
$referencialPath = $_ENV['REFERENCIAL_PATH'];
$referencialEasyPath = $_ENV['REFERENCIAL_EASY_PATH'];
$testIdFilePath = $_ENV['TEST_ID_FILE_PATH'];
$criteriaIdFilePathWithParticularCases = $_ENV['CRITERIA_FILE_WITH_PARTICULAR_CASES'];
$criteriaIdFilePathWithTechNote = $_ENV['CRITERIA_FILE_WITH_TECH_NOTE'];
$gitlaUrl = $_ENV['GITLAB_URL'];
$gitlabPrivateToken = $_ENV['GITLAB_PRIVATE_TOKEN'];
$gitlaProjectId = $_ENV['GITLAB_PROJECT_ID'];
# Optional configuration variables
if (isset($_ENV['START_ID']) && !empty($_ENV['START_ID'])) {
    $starId = $_ENV['START_ID'];
    # $starId = '3.3.2'; // do not import before test n° 3.3.2
}

// Process
if (!$filesystem->exists($testIdFilePath) && !is_writable($testIdFilePath)) {
    throw new Exception("File [ $testIdFilePath ] is not available");
}
$criteriaIdWithParticularCases = [];
if($filesystem->exists($criteriaIdFilePathWithParticularCases) && is_readable($criteriaIdFilePathWithParticularCases)){
    $criteriaIdWithParticularCases = array_flip(file("$criteriaIdFilePathWithParticularCases", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
}
$criteriaIdWithTechNote = [];
if($filesystem->exists($criteriaIdFilePathWithTechNote) && is_readable($criteriaIdFilePathWithTechNote)){
    $criteriaIdWithTechNote = array_flip(file("$criteriaIdFilePathWithTechNote", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
}

$notStartAtBeginning = false;
$process = true;
if (isset($starId)) {
    $notStartAtBeginning = true;
    $process = false;
}
$lines = file("$testIdFilePath");
foreach($lines as $id) {
    $id = trim($id);
    if ($notStartAtBeginning && $starId === $id) {
        $process = true;
    }
    $idPart = explode('.', $id);
    $topicId = $idPart[0];
    $criterionId = $idPart[1];
    $testId = $idPart[2];
    $idAnchor = "$topicId-$criterionId-$testId";
    $testMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/$testId/test_$id.md";
    if(!$filesystem->exists($testMarkdownFile) && !is_readable($testMarkdownFile)) {
        throw new Exception("File [ $testMarkdownFile ] is not available");
    }
    $testMarkdown = '';
    $testMarkdownLines = file("$testMarkdownFile");
    foreach($testMarkdownLines as $testMarkdownLine) {
        $testMarkdown .= "> $testMarkdownLine";
    }

    $labels = "Test";
    $particularCasesMarkdown = "";
    $techNoteMarkdown = "";
    if (isset($criteriaIdWithTechNote["$topicId.$criterionId"])) {
        $labels .= ", NT";
        $techNoteMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/criteria_$topicId.$criterionId"."_technicalNote.md";
        if(!$filesystem->exists($techNoteMarkdownFile) && !is_readable($techNoteMarkdownFile)) {
            throw new Exception("File [ $techNoteMarkdownFile ] is not available");
        }
        $techNoteMarkdown = "\n\n\n#### Notes techniques (critère $topicId.$criterionId)\n\n";
        $techNoteMarkdownLines = file("$techNoteMarkdownFile");
        foreach($techNoteMarkdownLines as $techNoteMarkdownLine) {
            $techNoteMarkdown .= "> $techNoteMarkdownLine";
        }
    }
    if (isset($criteriaIdWithParticularCases["$topicId.$criterionId"])) {
        $labels .= ", CP";
        $particularCasesMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/criteria_$topicId.$criterionId"."_particularCases.md";
        if(!$filesystem->exists($particularCasesMarkdownFile) && !is_readable($particularCasesMarkdownFile)) {
            throw new Exception("File [ $particularCasesMarkdownFile ] is not available");
        }
        $particularCasesMarkdown = "\n\n\n#### Cas particuliers (critère $topicId.$criterionId)\n\n";
        $particularCasesMarkdownLines = file("$particularCasesMarkdownFile");
        foreach($particularCasesMarkdownLines as $particularCasesMarkdownLine) {
            $particularCasesMarkdown .= "> $particularCasesMarkdownLine";
        }
    }
    if (strstr("$testMarkdown", "glossaire/#") !== false) {
        $labels .= ", GL";
    }
    echo $labels;

    // Prepare Issue data
    $post = [
        'title' => "$id",
        'labels'   => "$labels",
        'description' => "### Test $id

source : [**$idAnchor** sur numerique.gouv.fr](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-$idAnchor) (Rgaa v4) 

$testMarkdown $particularCasesMarkdown  $techNoteMarkdown


# Analyse pour Asqatasun

...

",
    ];

    $headers = [
        "PRIVATE-TOKEN: $gitlabPrivateToken",
    ];
    $url = "$gitlaUrl/api/v4/projects/$gitlaProjectId/issues";
    $ch = curl_init("$url");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    echo "\n\n\n-------------------> $id\n";
    if ($process === true) {
        $result = curl_exec($ch);
        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            throw new Exception("cURL error ({$errno}):\n {$error_message}");
        }

        $jsonResult = '';
        if (!empty($result)) {
            $jsonResult = json_encode(json_decode($result), JSON_PRETTY_PRINT);
        }

        $httpCode = curl_getinfo($ch)['http_code'];
//      print_r(curl_getinfo($ch));
        if ($httpCode !== 201) { // 201 Created
            $msg = "CURL - HTTP code [ $httpCode ] is not equal to: '201 Created'\n$jsonResult\n";
            throw new Exception($msg);
        }
        else {
            echo "HTTP code: 201 \"Created\"\n";
            print_r($result);
//          print_r($jsonResult);
        }
        curl_close($ch);
    }
    else {
        echo " --> no processing";
    }
}


