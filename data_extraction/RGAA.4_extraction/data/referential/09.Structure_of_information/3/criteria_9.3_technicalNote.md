Les attributs WAI-ARIA `role="list"` et `"listitem"` peuvent nécessiter l’utilisation des attributs WAI-ARIA `aria-setsize` et `aria-posinset` dans le cas où l’ensemble de la liste n’est pas disponible via le DOM généré au moment de la consultation.

Les attributs WAI-ARIA `role="tree"`, `"tablist"`, `"menu"`, `"combobox"` et `"listbox"` ne sont pas équivalents à une liste HTML `<ul>` ou `<ol>`.

Voir : [The roles model - list](https://www.w3.org/TR/wai-aria/#list)