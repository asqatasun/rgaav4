<?php
declare(strict_types = 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

// configuration variables
///////////////  /////////////////////////////////////////////////////////////////
define('_PATH_GIT_REPOSITORY',        './git.asqatasun');
define('_PATH_RGAA3',          _PATH_GIT_REPOSITORY .'/rules/rules-rgaa3.0');
define('_PATH_RGAA4',        _PATH_GIT_REPOSITORY .'/rules/rules-rgaa4.0');
define('_REF_RGAA3',          'rgaa30');
define('_REF_RGAA4',         'rgaa40');
////////////////////////////////////////////////////////////////////////////////
define('_CLASS',       '/src/main/java/org/asqatasun/rules');
define('_DOC',          _PATH_GIT_REPOSITORY . '/documentation/en/90_Rules');
define('_PATH_RGAA3_DOC',    _DOC   . '/rgaa3.0');
define('_PATH_RGAA4_DOC',    _DOC   . '/rgaa4.0');
define('_PATH_RGAA3_CLASS', _PATH_RGAA3  . _CLASS . '/'. _REF_RGAA3);
define('_PATH_RGAA4_CLASS', _PATH_RGAA4  . _CLASS . '/'. _REF_RGAA4);
////////////////////////////////////////////////////////////////////////////////
echo "-------------------------------------\n";
echo _PATH_RGAA3_CLASS."\n";
echo _PATH_RGAA4_CLASS."\n";
echo _PATH_RGAA3_DOC."\n";
echo _PATH_RGAA4_DOC."\n";
echo "-------------------------------------\n";

////////////////////////////////////////////////////////////////////////////////////////////////////////:

// Intialize tools
$filesystem = new Filesystem();

///////////////////////////////////////////////////
$rgaa3Rules =  extractDataFromJavaClass(_PATH_RGAA3_CLASS, "Rgaa30Rule");
$rgaa4Rules =  extractDataFromJavaClass(_PATH_RGAA4_CLASS, "Rgaa40Rule");
///////////////////////////////////////////////////
$rgaa3Rules = extractDataFromDocumentation($rgaa3Rules, _PATH_RGAA3_DOC);
$rgaa4Rules = extractDataFromDocumentation($rgaa4Rules, _PATH_RGAA4_DOC);
///////////////////////////////////////////////////
$rgaa3Meters = compute($rgaa3Rules);
$rgaa4Meters = compute($rgaa4Rules);
///////////////////////////////////////////////////
$filesystem->remove("build/");
$filesystem->dumpFile("build/rgaa3/rgaa3_rules.json", json_encode($rgaa3Rules, JSON_PRETTY_PRINT));
$filesystem->dumpFile("build/rgaa4/rgaa4_rules.json", json_encode($rgaa4Rules, JSON_PRETTY_PRINT));
$filesystem->dumpFile("build/rgaa3/rgaa3_meters.json", json_encode($rgaa3Meters, JSON_PRETTY_PRINT));
$filesystem->dumpFile("build/rgaa4/rgaa4_meters.json", json_encode($rgaa4Meters, JSON_PRETTY_PRINT));
$filesystem->dumpFile("build/rgaa3/rgaa3_rules.csv", exportCsvFormat($rgaa3Rules));
$filesystem->dumpFile("build/rgaa4/rgaa4_rules.csv", exportCsvFormat($rgaa4Rules));
///////////////////////////////////////////////////
print_r($rgaa3Rules);
print_r($rgaa3Meters);
print_r($rgaa4Rules);
print_r($rgaa4Meters);
echo count($rgaa3Rules)."\n";
echo count($rgaa4Rules)."\n";
///////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @param array $rules
 * @return array
 */
function compute(array $rules): array
{
    $meters = [
        'level' => [],
        'status' => [],
        'decision' => [],
        'result' => [],
        'with-markers' => 0,
        'abstractClass' => [],
    ];
    foreach ($rules as $compactId => $data){
        if(!empty($data['doc_withMarkers'])) {
            $meters['with-markers'] += 1;
        }
        //////////// Result  count
        if(!empty($data['doc_PASSED'])) {
            if (!isset($meters['result']["PASSED"])) {
                $meters['result']["PASSED"] = 0;
            }
            $meters['result']["PASSED"] += 1;
        }
        if(!empty($data['doc_FAILED'])) {
            if (!isset($meters['result']["FAILED"])) {
                $meters['result']["FAILED"] = 0;
            }
            $meters['result']["FAILED"] += 1;
        }
        if(!empty($data['doc_NA'])) {
            if (!isset($meters['result']["NA"])) {
                $meters['result']["NA"] = 0;
            }
            $meters['result']["NA"] += 1;
        }
        if(!empty($data['doc_NMI'])) {
            if (!isset($meters['result']["NMI"])) {
                $meters['result']["NMI"] = 0;
            }
            $meters['result']["NMI"] += 1;
        }
        //////////// abstractClass  count
        $abstractClass = $data['abstractClass'];
        if (!isset($meters['abstractClass']["$abstractClass"])) {
            $meters['abstractClass']["$abstractClass"] = 0;
        }
        $meters['abstractClass']["$abstractClass"] += 1;
        //////////// Status count
        $status = $data['src_status'];
        if (!isset($meters["status"]["$status"])) {
            $meters["status"]["$status"] = 0;
        }
        $meters["status"]["$status"] += 1;
        //////////// Level count
        $level = $data['doc_level'];
        if (!isset($meters["level"]["$level"])) {
            $meters["level"]["$level"] = 0;
        }
        $meters["level"]["$level"] += 1;
        //////////// decision count
        $decision = $data['doc_decision'];
        if(empty($decision)) {
            $decision = "-";
        }
        if (!isset($meters["decision"]["$decision"])) {
            $meters["decision"]["$decision"] = 0;
        }
        $meters["decision"]["$decision"] += 1;
//    [id] => 010101
//    [formatedId] => 01.01.01
//    [formatedIdShort] => 1.1.1
//    [id_topic] => 1
//    [id_criterion] => 1
//    [id_test] => 1
//    [abstractClass] => AbstractPageRuleWithSelectorAndCheckerImplementation
//    [src_status] => IS_TESTED
//    [src_withMarkers] =>
//    [src_PASSED] => 1
//    [src_FAILED] => 1
//    [src_NMI] => 0
//    [src_NA] => 0
//    [doc] => 01.Images/Rule-1-1-1.md
//    [doc_level] => A
//    [doc_decision] => Decidable
//    [doc_status] => IS_TESTED
//    [doc_withMarkers] =>
//    [doc_PASSED] => yes
//    [doc_FAILED] => yes
//    [doc_NMI] =>
//    [doc_NA] => yes
//    [error] => Array  ( )
    }
    return $meters;
}

/**
 * @param array $rules
 * @return array|string
 */
function exportCsvFormat( array $rules): string
{
    $csv = "";
    $csv .= '"ID";';
    $csv .= '"Topic";';
    $csv .= '"Criterion";';
    $csv .= '"Test";';
    $csv .= '"Level";';
    $csv .= '"Decision";';
    $csv .= '"doc_status";';
    $csv .= '"src_status";';
    $csv .= '"doc_withMarkers";';
    $csv .= '"doc_PASSED";';
    $csv .= '"doc_FAILED";';
    $csv .= '"doc_NMI";';
    $csv .= '"doc_NA";';
    $csv .= '"abstractClass";';
    $csv .= "\n";
    foreach ($rules as $compactId => $data){
        $csv .= '"'. $data['formatedId'] .'";';
        $csv .= '"'. $data['id_topic'] .'";';
        $csv .= '"'. $data['id_criterion'] .'";';
        $csv .= '"'. $data['id_test'] .'";';
        $csv .= '"'. $data['doc_level'] .'";';
        $csv .= '"'. $data['doc_decision'] .'";';
        $csv .= '"'. $data['doc_status'] .'";';
        $csv .= '"'. $data['src_status'] .'";';
        $csv .= '"'. $data['doc_withMarkers'] .'";';
        $csv .= '"'. $data['doc_PASSED'] .'";';
        $csv .= '"'. $data['doc_FAILED'] .'";';
        $csv .= '"'. $data['doc_NMI'] .'";';
        $csv .= '"'. $data['doc_NA'] .'";';
        $csv .= '"'. $data['abstractClass'] .'";';
        $csv .= "\n";
//    [id] => 010101
//    [formatedId] => 01.01.01
//    [formatedIdShort] => 1.1.1
//    [id_topic] => 1
//    [id_criterion] => 1
//    [id_test] => 1
//    [abstractClass] => AbstractPageRuleWithSelectorAndCheckerImplementation
//    [src_status] => IS_TESTED
//    [src_withMarkers] =>
//    [src_PASSED] => 1
//    [src_FAILED] => 1
//    [src_NMI] => 0
//    [src_NA] => 0
//    [doc] => 01.Images/Rule-1-1-1.md
//    [doc_level] => A
//    [doc_decision] => Decidable
//    [doc_status] => IS_TESTED
//    [doc_withMarkers] =>
//    [doc_PASSED] => yes
//    [doc_FAILED] => yes
//    [doc_NMI] =>
//    [doc_NA] => yes
//    [error] => Array  ( )
    }
    return $csv;
}


/**
 * @param array $rules
 * @param string $pathDocDir
 * @return array
 * @throws Exception
 */
function extractDataFromDocumentation (array $rules, string $pathDocDir):array
{
    $dir = new DirectoryIterator($pathDocDir);
    foreach ($dir as $dirInfo) {
        if (!$dirInfo->isDot() && is_dir($dirInfo->getPathname())) {
            $dirName =  $dirInfo->getFilename();
            $subDir = new DirectoryIterator($dirInfo->getPathname());
            foreach ($subDir as $fileinfo) {
                if (!$fileinfo->isDot()) {
                    $fileName =  $fileinfo->getFilename();
                    preg_match("#Rule-([\w-]+).md#", $fileName , $matches);
                    if(count($matches) > 0) {
                        $fileId = end($matches);
                        $compactId= getNormaliseID("$fileId", "-", ".");
                        $info = extractDataFromDocumentationFile($fileinfo);
                        $rules["$compactId"]["doc"] = "$dirName/$fileName";
                        $rules["$compactId"] = array_merge($rules["$compactId"], $info);

                        // Check
                        if(!isset($rules["$compactId"]['error'])) {
                            $rules["$compactId"]['error'] = [];
                        }
                        if ($rules["$compactId"]["src_status"] !== $info["doc_status"]) {
                            $rules["$compactId"]['error'][] = "DIFF src_status vs doc_status";
                            // throw  new Exception("Doc [$fileName] file - src_status vs doc_status");
                        }
                        if ($rules["$compactId"]["src_withMarkers"] !== $info["doc_withMarkers"]) {
                            $rules["$compactId"]['error'][] = "DIFF src_withMarkers vs doc_withMarkers";
//                            echo file_get_contents($fileinfo->getPathname());
//                            print_r( $rules["$compactId"]);
//                            throw  new Exception("Doc [$fileName] file - src_withMarkers vs doc_withMarkers");
                        }

                    }

                }
            }
        }
    }
    return $rules;
}

/**
 * @param DirectoryIterator $fileinfo
 * @return array
 * @throws Exception
 */
function extractDataFromDocumentationFile(DirectoryIterator $fileinfo):array
{
    $data = [];
    $fileName =  $fileinfo->getFilename();
    $fileContent = file_get_contents($fileinfo->getPathname());

    // Extract LEVEL
    $level_A = substr_count("$fileContent", "**A**");
    $level_AA = substr_count("$fileContent", "**AA**");
    $level_AAA = substr_count("$fileContent", "**AAA**");
    $level = "";
    if($level_A === 1) { $level = "A"; }
    if($level_AA === 1) { $level = "AA"; }
    if($level_AAA === 1) { $level = "AAA"; }
    if(empty($level)) {
        throw  new Exception("Doc [$fileName] file - Level not found");
    }

    // Extract Decision + WITH_MARKERS
    $withMarker = "";
    $semiDecidable = substr_count("$fileContent", "**Semi-Decidable**");
    $decidable = substr_count("$fileContent", "**Decidable**");
    $decidableWithMarker = substr_count("$fileContent", "**Decidable with marker**");
    $decision  = "";
    if($semiDecidable  === 1) { $decision = "Semi-Decidable"; }
    if($decidable  === 1) { $decision = "Decidable"; }
    if($decidableWithMarker === 1) { $decision = "Decidable with marker"; $withMarker = "WITH_MARKERS"; }
    if(empty($decision)) {
        $decidableTodo = substr_count("$fileContent", "@@@TODO");
        if($decidableTodo  === 0) {
            echo $fileContent;
            throw  new Exception("Doc [$fileName] file - Level not found");
        }
    }

    // Complete WITH_MARKERS
    if(empty($withMarker)) {
        $countWithMarker = substr_count(strtoupper("$fileContent"), strtoupper("MARKER"));
        if($countWithMarker > 0) {
            $withMarker = "WITH_MARKERS";
        }
    }



    // Extract msg type
    $resultPassed = "";
    $resultFailed = "";
    $resultNmi = "";
    $resultNa = "";
    if (substr_count("$fileContent", "## Passed") > 0) {
        $resultPassed = 'yes';
    }
    if (substr_count("$fileContent", "## Failed") > 0) {
        $resultFailed = 'yes';
    }
    if (substr_count("$fileContent", "## Not Applicable") > 0) {
        $resultNa = 'yes';
    }
    if (substr_count("$fileContent", "## Pre-qualified") > 0) {
        $resultNmi = 'yes';
    }
    else if (substr_count("$fileContent", "## Pre-Qualified") > 0) {
        $resultNmi = 'yes';
    }

    $tested = "NOT_TESTED";
    if (!empty($resultPassed) || !empty($resultFailed) || !empty($resultNmi) || !empty($resultNa)) {
        $tested = "IS_TESTED";
        if (substr_count("$fileContent", "No-check rule") > 0) {
            echo $fileContent;
            throw  new Exception("Doc [$fileName] file - No-check rule vs IS_TESTED");
        }
    }

    // Compute data
    $data["doc_level"] = $level;
    $data["doc_decision"] = $decision;
    $data["doc_status"] = $tested;
    $data["doc_withMarkers"] = $withMarker;
    $data["doc_PASSED"] = $resultPassed;
    $data["doc_FAILED"] = $resultFailed;
    $data["doc_NMI"] = $resultNmi;
    $data["doc_NA"] = $resultNa;
    return $data;
}



/**
 * Extract data from java rule class
 *
 * @param string $pathClassDir     ex: "asqatasun/rules/rules-rgaa3.0/src/main/java/org/asqatasun/rules/rgaa30"
 * @param string $filePrefixClass  ex: "Rgaa30Rule"
 * @return array
 */
function extractDataFromJavaClass (string $pathClassDir, string $filePrefixClass):array
{
    $dir = new DirectoryIterator($pathClassDir);
    $rules = [];
    foreach ($dir as $fileinfo) {
        if (!$fileinfo->isDot()) {
            $fileName =  $fileinfo->getFilename();
            $fileContent = file_get_contents($fileinfo->getPathname());

            $compactId = str_replace(".java", "", "$fileName");
            $compactId = str_replace("$filePrefixClass", "","$compactId");
            $idStr = getIdFromCompactID("$compactId");
            $idInt = getIdFromCompactID("$compactId", false);
            $formatedId = "${idStr['topic']}.${idStr['criterion']}.${idStr['test']}";
            $formatedIdShort = "${idInt['topic']}.${idInt['criterion']}.${idInt['test']}";

            preg_match("#public(\s+)class(\s+)$filePrefixClass$compactId(\s+)extends(\s+)(\w+)#", $fileContent , $matches);
            if (count($matches) > 1) {
                $abstractClass = end($matches);
                $status = "NOT_TESTED";
                if($abstractClass !== "AbstractNotTestedRuleImplementation") {
                    $status = "IS_TESTED";
                }
                $withMarkers = "";
                if($abstractClass === "AbstractMarkerPageRuleImplementation") {
                    $withMarkers = "WITH_MARKERS";
                }


                $countPassed = substr_count("$fileContent", "TestSolution.PASSED");
                $countFailed = substr_count("$fileContent", "TestSolution.FAILED");
                $countNmi = substr_count("$fileContent", "TestSolution.NEED_MORE_INFO");
                $countNa = substr_count("$fileContent", "TestSolution.NOT_APPLICABLE");

                $countPassed += substr_count("$fileContent", "ImmutablePair<>(PASSED");
                $countFailed += substr_count("$fileContent", "ImmutablePair<>(FAILED");
                $countNmi += substr_count("$fileContent", "ImmutablePair<>(NEED_MORE_INFO");
                $countNa += substr_count("$fileContent", "ImmutablePair<>(NOT_APPLICABLE");

                $rules ["$formatedId"]['id'] = $compactId;
                $rules ["$formatedId"]['formatedId'] = $formatedId;
                $rules ["$formatedId"]['formatedIdShort'] = $formatedIdShort;
                $rules ["$formatedId"]['id_topic'] = $idInt['topic'];
                $rules ["$formatedId"]['id_criterion'] = $idInt['criterion'];
                $rules ["$formatedId"]['id_test'] = $idInt['test'];
                $rules ["$formatedId"]['abstractClass'] = $abstractClass;
                $rules ["$formatedId"]['src_status'] = $status;
                $rules ["$formatedId"]['src_withMarkers'] = $withMarkers;
                $rules ["$formatedId"]['src_PASSED'] = $countPassed;
                $rules ["$formatedId"]['src_FAILED'] = $countFailed ;
                $rules ["$formatedId"]['src_NMI'] = $countNmi;
                $rules ["$formatedId"]['src_NA'] = $countNa;
                // echo "$fileName - $compactId -$formatedId  - $formatedIdShort - $abstractClass\n";
            }
            else {
                echo "$fileName\n";
            }
        }
    }
    ksort($rules);
    return $rules;
}



/**
 * Returns decomposed ID [1, 4, 2] or ['01', '04', '02']
 * from a compact ID (ex: 010402)
 *
 * @param string $compactId     ex: 010402
 * @param bool $keepZeroPrefix  default is true
 * @return int[]|string[]       ex: [1, 4, 2] with $keepZeroPrefix to false, else ['01', '04', '02']
 */
function getIdFromCompactID(string $compactId, bool $keepZeroPrefix = true):array {
    $topic = substr("$compactId", 0, 2);
    $criterion = substr("$compactId", 2, 2);
    $test = substr("$compactId", 4, 2);
    if ($keepZeroPrefix) {
        return [
            "topic" => "$topic",
            "criterion" => "$criterion",
            "test" => "$test",
        ];
    }
    return [
        "topic" => (int) $topic,
        "criterion" => (int) $criterion,
        "test" => (int) $test,
    ];
}

/**
 * @param string $ID
 * @param string $delimiter
 * @param string $finalDelimiter
 * @return string
 */
function getIDformated(string $ID, string $delimiter = '.', string $finalDelimiter = '-'):string
{
    $tab        = getIDinTab($ID, $delimiter);
    $theme      = $tab[0];
    $criterion  = $tab[1];
    $test       = $tab[2];
    return $theme .$finalDelimiter.$criterion.$finalDelimiter.$test ;
}

function getIDinTab($txt,$delimiter = '.')
{
    $tab    = explode($delimiter,$txt);
    return $tab;
}

/**
 * @param string $ID
 * @param string $delimiter
 * @param string $finalDelimiter
 * @return string
 */
function getNormaliseID(string $ID, string $delimiter = '.', string $finalDelimiter = ''):string
{
    $tab        = getIDinTab($ID, $delimiter);
    $theme      = str_pad($tab[0], 2, "00", STR_PAD_LEFT);
    $criterion  = str_pad($tab[1], 2, "00", STR_PAD_LEFT);
    $test       = str_pad($tab[2], 2, "00", STR_PAD_LEFT);
    return $theme . $finalDelimiter . $criterion . $finalDelimiter . $test ;
}
