# RGAA 4.0 - Rule 12.5.2

## Summary

No-check rule

## Business description

### Criterion

[12.5](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-12-5)

### Test

[12.5.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-12-5-2)

### Description

> Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), la fonctionnalité vers le [moteur de recherche](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web) est-elle située à la même place dans la présentation ?

### Level

**AA**


## Technical description

### Scope

**Page**

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 12.5.2](https://gitlab.com/asqatasun/Asqatasun/-/tree/v5/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule120502/)
- [Unit test file for rule 12.5.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule120502Test.java)
- [Class file for rule 12.5.2](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule120502.java)


