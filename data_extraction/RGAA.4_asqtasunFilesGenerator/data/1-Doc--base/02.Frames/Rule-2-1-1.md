# RGAA 4.0 - Rule 2.1.1

## Summary

No-check rule

## Business description

### Criterion

[2.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-2-1)

### Test

[2.1.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-2-1-1)

### Description

> Chaque cadre (balise `<iframe>` ou `<frame>`) a-t-il un attribut `title` ?

### Level

**A**


## Technical description

### Scope

**Page**

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 2.1.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/v5/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule020101/)
- [Unit test file for rule 2.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule020101Test.java)
- [Class file for rule 2.1.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule020101.java)


