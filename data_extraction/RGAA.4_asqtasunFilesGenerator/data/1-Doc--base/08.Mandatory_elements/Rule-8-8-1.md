# RGAA 4.0 - Rule 8.8.1

## Summary

No-check rule

## Business description

### Criterion

[8.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-8-8)

### Test

[8.8.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-8-8-1)

### Description

> Pour chaque page web, le code de langue de chaque [changement de langue](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#changement-de-langue) vérifie-t-il ces conditions ?
> 
> * Le code de langue est valide.
> * Le code de langue est pertinent.

### Level

**AA**


## Technical description

### Scope

**Page**

### Decision level

@@@TODO


## Algorithm

### Selection

None

### Process

None

### Analysis

#### Not Tested

In all cases


## Files

- [TestCases files for rule 8.8.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/v5/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule080801/)
- [Unit test file for rule 8.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule080801Test.java)
- [Class file for rule 8.8.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/v5/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule080801.java)


