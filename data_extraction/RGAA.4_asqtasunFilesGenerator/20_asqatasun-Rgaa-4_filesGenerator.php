<?php
declare(strict_types = 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use function Symfony\Component\String\u; // the u() function creates Unicode strings

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Intialize tools
$dotenv = new Dotenv();
$filesystem = new Filesystem();
$httpClient = HttpClient::create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Get configuration variables from .env.dist file
$envFile = __DIR__ . '/.env.dist';
if (!$filesystem->exists("$envFile") || !is_readable("$envFile")) {
    throw new Exception("[ /.env ] file is not available.");
}
$dotenv->load("$envFile");
$dataPath = $_ENV['DATA_PATH'];
$referencialPath = $_ENV['REFERENCIAL_PATH'];
$referencialEasyPath = $_ENV['REFERENCIAL_EASY_PATH'];
$testIdFilePath = $_ENV['TEST_ID_FILE_PATH'];
$criteriaIdsLevel2FilePath = $_ENV['CRITERIA_ID_LEVEL_AA_FILE_PATH'];
$criteriaIdFilePathWithParticularCases = $_ENV['CRITERIA_FILE_WITH_PARTICULAR_CASES'];
$criteriaIdFilePathWithTechNote = $_ENV['CRITERIA_FILE_WITH_TECH_NOTE'];
$officialDocumentationUrl = $_ENV['OFFICIAL_DOCUMENTATION_URL'];
$RuleDesignDocUrlPrefix = $_ENV['RULE_DESIGN_DOCUMENTATION_URL'];
$RuleDesignDocFileExt = $_ENV['RULE_DESIGN_DOCUMENTATION_FILE_EXTENSION'];

// Complementary configuration
$buildPath        = 'build/'.date('Y-m-d_H\hi');
$srcPath          = "$buildPath/0-source";
$docBasePath      = "$buildPath/1-Doc--base";
$testCaseBasePath = "$buildPath/3-TestCase--base";
$javaTestFileBasePath = "$buildPath/4-TestJava--base";
$javaFileBasePath = "$buildPath/5-MainJava--base";
$branch = "v5";
$testCaseUrlPrefix  = "https://gitlab.com/asqatasun/Asqatasun/-/tree/$branch/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40";
$unitTestFileUrlPrefix  = "https://gitlab.com/asqatasun/Asqatasun/-/blob/$branch/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40";
$javaFileUrlPrefix  = "https://gitlab.com/asqatasun/Asqatasun/-/blob/$branch/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40";

// Configuration: documentation directories
$topicDirectories     = Array();
$topicDirectories[1]  = '01.Images';
$topicDirectories[2]  = '02.Frames';
$topicDirectories[3]  = '03.Colours';
$topicDirectories[4]  = '04.Multimedia';
$topicDirectories[5]  = '05.Tables';
$topicDirectories[6]  = '06.Links';
$topicDirectories[7]  = '07.Scripts';
$topicDirectories[8]  = '08.Mandatory_elements';
$topicDirectories[9]  = '09.Structure_of_information';
$topicDirectories[10] = '10.Presentation_of_information';
$topicDirectories[11] = '11.Forms';
$topicDirectories[12] = '12.Navigation';
$topicDirectories[13] = '13.Consultation';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Clean up directories
$filesystem->remove("$buildPath");

// Load data from files
if (!$filesystem->exists($testIdFilePath) && !is_writable($testIdFilePath)) {
    throw new Exception("File [ $testIdFilePath ] is not available");
}
else {
    $testIds = file("$testIdFilePath");
}
$criteriaIdsLevel2  = [];
if($filesystem->exists($criteriaIdsLevel2FilePath) && is_readable($criteriaIdsLevel2FilePath)){
    $criteriaIdsLevel2 = array_flip(file("$criteriaIdsLevel2FilePath", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
}
$criteriaIdWithParticularCases = [];
if($filesystem->exists($criteriaIdFilePathWithParticularCases) && is_readable($criteriaIdFilePathWithParticularCases)){
    $criteriaIdWithParticularCases = array_flip(file("$criteriaIdFilePathWithParticularCases", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
}
$criteriaIdWithTechNote = [];
if($filesystem->exists($criteriaIdFilePathWithTechNote) && is_readable($criteriaIdFilePathWithTechNote)){
    $criteriaIdWithTechNote = array_flip(file("$criteriaIdFilePathWithTechNote", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
}


// Processing data
$topicsReadmeData = [];
$sql = '';
foreach($testIds as $id) {

    // Extract and compute Ids (topic, criterion, test)
    $id = trim($id);
    $idPart = explode('.', $id);
    $topicId = $idPart[0];
    $criterionId = $idPart[1];
    $criterionIdFull = "$topicId.$criterionId";
    $criterionIdCompact = '';
    $criterionIdCompact .= str_pad("$topicId", 2, "0", STR_PAD_LEFT);
    $criterionIdCompact .= str_pad("$criterionId", 2, "0", STR_PAD_LEFT);
    $criterionIdAnchor = "$topicId-$criterionId";
    $testId = $idPart[2];
    $testIdFull = "$topicId.$criterionId.$testId";
    $testIdCompact = '';
    $testIdCompact .= str_pad("$topicId", 2, "0", STR_PAD_LEFT);
    $testIdCompact .= str_pad("$criterionId", 2, "0", STR_PAD_LEFT);
    $testIdCompact .= str_pad("$testId", 2, "0", STR_PAD_LEFT);
    $testIdAnchor = "$topicId-$criterionId-$testId";

    // Compute some URLs
    $ruleSpecUrl = "$officialDocumentationUrl#test-$testIdAnchor";
    $RuleDesignDocUrl = "$RuleDesignDocUrlPrefix/${topicDirectories[$topicId]}/Rule-$testIdAnchor.$RuleDesignDocFileExt";
    $testCaseUrl = "$testCaseUrlPrefix/Rgaa40Rule$testIdCompact/";
    $unitTestFileUrl = "$unitTestFileUrlPrefix/Rgaa40Rule$testIdCompact"."Test.java";
    $javaFileUrl = "$javaFileUrlPrefix/Rgaa40Rule$testIdCompact.java";

    // Add SQL
    $sql .= "-- UPDATE TEST SET `No_Process`=b'0' WHERE Cd_Test='Rgaa40-$testIdAnchor';\n";

    // Prepare README files (rule design documentation)
    $topicReadmeMarkdown = "* [Rule $testIdFull](Rule-$testIdAnchor.md)\n";
    if(!isset($topicsReadmeData[$topicId][$criterionIdFull])){
        $criteriaMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/criteria_$criterionIdFull.md";
        if(!$filesystem->exists($criteriaMarkdownFile) && !is_readable($criteriaMarkdownFile)) {
            throw new Exception("File [ $criteriaMarkdownFile ] is not available");
        }
        $criteriaMarkdown = '';
        $criteriaMarkdownLines = file("$criteriaMarkdownFile");
        foreach($criteriaMarkdownLines as $criteriaMarkdownLine) {
            $criteriaMarkdown .= "> $criteriaMarkdownLine";
        }
        $topicsReadmeData[$topicId][$criterionIdFull] = "\n## Criterion $criterionIdFull\n\n";
        $topicsReadmeData[$topicId][$criterionIdFull] .= "$criteriaMarkdown\n\n";
        $topicsReadmeData[$topicId][$criterionIdFull] .= $topicReadmeMarkdown;
    }
    else {
        $topicsReadmeData[$topicId][$criterionIdFull] .= $topicReadmeMarkdown;
    }

    // Prepare Markdown content (description, technical note and particular cases)
    $testMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/$testId/test_$id.md";
    if(!$filesystem->exists($testMarkdownFile) && !is_readable($testMarkdownFile)) {
        throw new Exception("File [ $testMarkdownFile ] is not available");
    }
    $testMarkdown = '';
    $testMarkdownLines = file("$testMarkdownFile");
    foreach($testMarkdownLines as $testMarkdownLine) {
        $testMarkdown .= "> $testMarkdownLine";
    }
    $techNoteMarkdown = "";
    if (isset($criteriaIdWithTechNote["$topicId.$criterionId"])) {
        $techNoteMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/criteria_$topicId.$criterionId"."_technicalNote.md";
        if(!$filesystem->exists($techNoteMarkdownFile) && !is_readable($techNoteMarkdownFile)) {
            throw new Exception("File [ $techNoteMarkdownFile ] is not available");
        }
        $techNoteMarkdown = "";
        $techNoteMarkdownLines = file("$techNoteMarkdownFile");
        foreach($techNoteMarkdownLines as $techNoteMarkdownLine) {
            $techNoteMarkdown .= "> $techNoteMarkdownLine";
        }
    }
    $particularCasesMarkdown = "";
    if (isset($criteriaIdWithParticularCases["$topicId.$criterionId"])) {
        $particularCasesMarkdownFile = "$referencialEasyPath/$topicId/$criterionId/criteria_$topicId.$criterionId"."_particularCases.md";
        if(!$filesystem->exists($particularCasesMarkdownFile) && !is_readable($particularCasesMarkdownFile)) {
            throw new Exception("File [ $particularCasesMarkdownFile ] is not available");
        }
        $particularCasesMarkdown = "";
        $particularCasesMarkdownLines = file("$particularCasesMarkdownFile");
        foreach($particularCasesMarkdownLines as $particularCasesMarkdownLine) {
            $particularCasesMarkdown .= "> $particularCasesMarkdownLine";
        }
    }

    // Prepare TestCases files for current test
    $ruleContentInHtmlPath = "$referencialEasyPath/$topicId/$criterionId/$testId/test_$testIdFull--HTML.html";
    if (!$filesystem->exists($ruleContentInHtmlPath) && !is_readable($ruleContentInHtmlPath)) {
        throw new Exception("File [ $testIdFilePath ] is not available");
    }
    $ruleContentLines = file("$ruleContentInHtmlPath", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $ruleContentInHtml = '';
    foreach($ruleContentLines as $ruleContentHtmlLine) {
        $ruleContentInHtml .= "            ".u($ruleContentHtmlLine)->trim()->toString()."\n";
    }
    $ruleContentInHtml = u($ruleContentInHtml)->trim()->toString();
    $basicContentOfTestCase = file_get_contents("ressource/rgaa4.0/template_testCase_HTML5.html");
    $basicContentOfTestCase = str_replace('###[ID_v2]###',$testIdFull,$basicContentOfTestCase);
    $basicContentOfTestCase = str_replace('###[test-detail]###',"$ruleContentInHtml","$basicContentOfTestCase");

    // Save TestCases files for current test
    // ---> "test/resources/testcases/(...).dist.html"  (example: "Rgaa40.Test.1.1.1-1Passed-01.dist.html")
    $testCasePathPrefix = "$testCaseBasePath/Rgaa40Rule$testIdCompact/Rgaa40.Test.$testIdFull";
    $contentOfTestCase = str_replace('###[state]###','Passed',$basicContentOfTestCase);
    $filesystem->dumpFile("$testCasePathPrefix-1Passed-01.dist.html",$contentOfTestCase);
    $contentOfTestCase = str_replace('###[state]###','Failed',$basicContentOfTestCase);
    $filesystem->dumpFile("$testCasePathPrefix-2Failed-01.dist.html",$contentOfTestCase);
    $contentOfTestCase = str_replace('###[state]###','NMI',$basicContentOfTestCase);
    $filesystem->dumpFile("$testCasePathPrefix-3NMI-01.dist.html",$contentOfTestCase);
    $contentOfTestCase = str_replace('###[state]###','NA',$basicContentOfTestCase);
    $filesystem->dumpFile("$testCasePathPrefix-4NA-01.dist.html",$contentOfTestCase);

    $filesystem->copy("$testCasePathPrefix-3NMI-01.dist.html", "$testCasePathPrefix-3NMI-01.html");


    // Save java file for current test
    // ---> "main/(...).java"  (example: "Rgaa40Rule010301.java")
    $javaFileContent = file_get_contents("ressource/rgaa4.0/template_mainRule.java");
    $javaFileContent = str_replace('###[ID_v2]###',         $testIdFull,    $javaFileContent);
    $javaFileContent = str_replace('###[ID_v3]###',         $testIdCompact, $javaFileContent);
    $javaFileContent = str_replace('###[URL_rule_spec]###', $ruleSpecUrl,   $javaFileContent);
    $javaFileContent = str_replace('###[URL_rule_algo]###', $RuleDesignDocUrl,   $javaFileContent);
    $javaFilePath = "$javaFileBasePath/Rgaa40Rule$testIdCompact.java";
    $filesystem->dumpFile("$javaFilePath", "$javaFileContent");

    // Save java test file for current test
    // ---> "test/(...)Test.java"  (example: "Rgaa40Rule010301Test.java")
    $javaTestFileContent = file_get_contents("ressource/rgaa4.0/template_testRule.java");
    $javaTestFileContent = str_replace('###[ID_v1]###',         $testIdAnchor,  $javaTestFileContent);
    $javaTestFileContent = str_replace('###[ID_v2]###',         $testIdFull,    $javaTestFileContent);
    $javaTestFileContent = str_replace('###[ID_v3]###',         $testIdCompact, $javaTestFileContent);
    $javaTestFileContent = str_replace('###[URL_rule_spec]###', $ruleSpecUrl,   $javaTestFileContent);
    $javaTestFileContent = str_replace('###[URL_rule_algo]###', $RuleDesignDocUrl,   $javaTestFileContent);
    $javaTestFilePath = "$javaTestFileBasePath/Rgaa40Rule$testIdCompact".'Test.java';
    $filesystem->dumpFile("$javaTestFilePath", "$javaTestFileContent");

    // Prepare documentation file for current test
    // @@@@TODO create and use a template file
    $level = "A";
    if (isset($criteriaIdsLevel2[$criterionIdFull])) {
        $level = "AA";
    }
    $docMarkdown  = '';
    $docMarkdown .= "# RGAA 4.0 - Rule $testIdFull\n\n";
    $docMarkdown .= "## Summary\n\n";
    $docMarkdown .= "No-check rule\n\n";
    $docMarkdown .= "## Business description\n\n";
    $docMarkdown .= "### Criterion\n\n";
    $docMarkdown .= "[$criterionIdFull]($officialDocumentationUrl#crit-$criterionIdAnchor)\n\n";
    $docMarkdown .= "### Test\n\n";
    $docMarkdown .= "[$testIdFull]($officialDocumentationUrl#test-$testIdAnchor)\n\n";
    $docMarkdown .= "### Description\n\n";
    $docMarkdown .= "$testMarkdown\n\n";
    if (!empty($particularCasesMarkdown)) {
        $docMarkdown .= "#### Particular cases (criterion $topicId.$criterionId)\n\n";
        $docMarkdown .= "$particularCasesMarkdown\n\n";
    }
    if (!empty($techNoteMarkdown)) {
        $docMarkdown .= "#### Technical notes (criterion $topicId.$criterionId)\n\n";
        $docMarkdown .= "$techNoteMarkdown\n\n";
    }
    $docMarkdown .= "### Level\n\n";
    $docMarkdown .= "**$level**\n\n\n";
    $docMarkdown .= "## Technical description\n\n";
    $docMarkdown .= "### Scope\n\n";
    $docMarkdown .= "**Page**\n\n";
    $docMarkdown .= "### Decision level\n\n";
    $docMarkdown .= "@@@TODO\n\n\n";
    $docMarkdown .= "## Algorithm\n\n";
    $docMarkdown .= "### Selection\n\n";
    $docMarkdown .= "None\n\n";
    $docMarkdown .= "### Process\n\n";
    $docMarkdown .= "None\n\n";
    $docMarkdown .= "### Analysis\n\n";
    $docMarkdown .= "#### Not Tested\n\n";
    $docMarkdown .= "In all cases\n\n\n";
    $docMarkdown .= "## Files\n\n";
    $docMarkdown .= "- [TestCases files for rule $testIdFull]($testCaseUrl)\n";
    $docMarkdown .= "- [Unit test file for rule $testIdFull]($unitTestFileUrl)\n";
    $docMarkdown .= "- [Class file for rule $testIdFull]($javaFileUrl)\n";
    $docMarkdown .= "\n\n";

    // Save documentation file for current test
    $topicDirectory     = $topicDirectories[$topicId];
    $DocPathEnd    = "$topicDirectory/Rule-$testIdAnchor.md";
    $DocFile = "$docBasePath/$DocPathEnd";
    $filesystem->dumpFile("$DocFile", "$docMarkdown");
}

// Save SQL file
$filesystem->dumpFile("$buildPath/SQL_enable-rule.sql",$sql);

// Finalize creation of README files (rule design documentation)
foreach ($topicsReadmeData as $topicId => $topicData) {
    $topicDirectory     = $topicDirectories[$topicId];
    $topicTitle         = substr($topicDirectory, 3);
    $topicTitle         = str_replace('_',' ',$topicTitle);
    $topicMarkdown = "# RGAA 4.0 - Theme $topicId: $topicTitle\n";
    foreach ($topicData as $criteriaFullId => $criteriaData){
        $topicMarkdown .=  $criteriaData;
    }
    $topicMarkdown .= "\n";
    $topicMarkdownFile = "$docBasePath/$topicDirectory/README.md";
    $filesystem->dumpFile("$topicMarkdownFile", "$topicMarkdown");
}


// Backup files
$filesystem->remove("./data");
$filesystem->remove("./source");
$filesystem->mirror($buildPath, "./data");
