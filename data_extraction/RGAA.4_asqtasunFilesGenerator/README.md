
# RGAA.4 - Asqatasun files generator (`*.java`, `*.html`, `*.md`)

- Needed: [RGAA.4 - Data Extraction](../RGAA.4_extraction/)
- Creates following files in `data` directory:
  - java files
  - Unit test files
  - TestCase files 
  - rule design documentation files

```bash
composer install
php 20_asqatasun-Rgaa-4_filesGenerator.php 
```
