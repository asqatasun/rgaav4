/**
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.rules.rgaa40;

import org.asqatasun.ruleimplementation.AbstractNotTestedRuleImplementation;

/**
 * Implementation of rule ###[ID_v2]### (referential RGAA 4.0)
 *
 * For more details about implementation, refer to <a href="###[URL_rule_algo]###">rule ###[ID_v2]### design page</a>.
 * @see <a href="###[URL_rule_spec]###">###[ID_v2]### rule specification</a>
 */
public class Rgaa40Rule###[ID_v3]### extends AbstractNotTestedRuleImplementation {

    /**
     * Default constructor
     */
    public Rgaa40Rule###[ID_v3]###() {
        super();
    }

}
