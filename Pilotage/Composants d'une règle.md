# Composants d'une règle

* sélection / détection d'un élément
* check (vérification d'une valeur)
* marqueurs (tableaux, captcha...)
* helper à créer (par exemple : doctype, intitulé de champ, alternative textuelle...)
* "check on page load", ce qui nécessite de faire tourner un algo JS au chargement de la pag (exemple: récupération des couleurs, récupération de la position du doctype)
