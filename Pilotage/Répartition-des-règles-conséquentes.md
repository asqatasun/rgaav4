# Répartition des règles conséquentes

## Répartition par thématique

Thématique | Nombre de conséquentes
----------:|----------------------:
01 | 48
02 | 0 
03 | 0 
04 | 0 
05 | 3
06 | 6
07 | 4
08 | 1
09 | 1
10 | 1
11 | 16
12 | 4
13 | 0

Total conséquentes = 84

## Croisement conséquente & implementé RGAAv3

* conséquente & implementé RGAAv3 = 57
