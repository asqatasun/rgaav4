-- Export
SELECT Rule_Archive_Name,
       Label,
       No_Process,
       Id_Level,
       Cd_Test,
       Rank
FROM  TEST
WHERE Rule_Archive_Name = 'rgaa30'
ORDER BY Rank;

-- Stat - Processs or not by Asqatasun
SELECT Rule_Archive_Name,
       No_Process, 
       count(*)
FROM   TEST
WHERE  Rule_Archive_Name = 'rgaa30'
GROUP BY  No_Process ;

-- Stat - No_Processs by Asqatasun (group by level)
SELECT Id_Level, 
       count(*)
FROM   TEST
WHERE  Rule_Archive_Name = 'rgaa30'
  AND  No_Process IS TRUE
GROUP BY  Id_Level 

-- Stat - Process by Asqatasun (group by level)
SELECT Id_Level, 
       count(*)
FROM   TEST
WHERE  Rule_Archive_Name = 'rgaa30'
  AND  No_Process IS FALSE
GROUP BY  Id_Level 

