# RGAA.4 Referential

## CSV format

- column separator: `;`
- column surround: `'`

example:
```csv
TopicID;Topic;Level;CriterionID;TestID;ParticularCases;TechnicalNote;CriterionUseGlossary;TestUseGlossary;CriterionGlossaryAnchors;TestGlossaryAnchors;Criterion;test;
'1';'Images';'A';'1.1';'1.1.1';'';'';'2';'2';'#image-porteuse-d-information #alternative-textuelle-image';'#image-porteuse-d-information #alternative-textuelle-image';'Chaque image porteuse d’information a-t-elle une alternative textuelle ?';'Chaque image (balise <img> ou balise possédant l’attribut WAI-ARIA role="img") porteuse d’information a-t-elle une alternative textuelle ?';
```
