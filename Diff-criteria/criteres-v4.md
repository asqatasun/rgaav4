# File `criteres-v4.json`

## Creation

```shell script
jq 'del(.topics[].criteria[].criterium.references, .topics[].criteria[].criterium.tests, .topics[].criteria[].criterium.particularCases, .topics[].criteria[].criterium.technicalNote)' ../RGAAv4/criteres.json > criteres-v4.json
```

## Content

* Only *criteria* from RGAAv4
