# Analyse-differences-criteres.md

## Méthodologie

* la comparaison est effectuée **DEPUIS** le rgaav3 **VERS** le rgaav4
* Dans un premier temps, l'ajout de lien vers le glossaire (au sein d'un intitulé de critère) n'est **PAS** considéré comme une différence.

## Vocabulaire

* un critère *supprimé* existait en v3 mais n'existe plus en v4
* un critère *renuméroté* possède un même intitulé en v3 et en v4 (rappel : l'ajout de lien vers le glossaire n'est pas considéré comme une différence), mais son numéro a changé.
* un critère *modifié* possède un intitulé différent en v3 et en v4

##  Thématique 1 Images

 Critères | RGAAv3 | RGAAv4 | 
----------|-------:|-------:|
Nombre    |     10 |      9 |

### Critères supprimés

* 1.9

### Critères renumérotés

* 1.10 devient 1.9

### Critères modifiés

* 1.1
* 1.2

### Critères identiques

Tous les autres soit :

* 1.3
* 1.4
* 1.5
* 1.6
* 1.7
* 1.8

##  Thématique 2 Cadres

 Critères | RGAAv3 | RGAAv4 | 
----------|-------:|-------:|
Nombre    |     2 |      2 |

### Critères supprimés

Aucun

### Critères renumérotés

Aucun

### Critères modifiés

* 2.1
* 2.2

### Critères identiques

Aucun

##  Thématique 3 Couleurs

 Critères | RGAAv3 | RGAAv4 | 
----------|-------:|-------:|
Nombre    |     4 |      3 |

### Critères supprimés

* 3.2
* 3.4

### Critères renumérotés

* 3.3 devient 3.2

### Critères modifiés

Aucun

### Critères identiques

* 3.1

