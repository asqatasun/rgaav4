# RGAA v3 "rebuild"

This directory is made of the RGAAv3 content, organized in the same way the RGAAv4 is.
That is to say: criterias, test, technical notes, glossary and special cases are structured in JSON files,
so that it is easy to `diff` them between RGAAv3 and RGAAv4. 
