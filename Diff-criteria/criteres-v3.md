# File `criteres-v3.json`

## Creation

* Once `criteres-v4.json` is created: `cp criteres-v4.json criteres-v3.json`
* All criteria are manually replaced by those from RGAAv3, and put in Markdown format (converted with https://convert-tool.com/conversion/html-to-markdown) 

## Content

* Only *criteria* from RGAAv4
